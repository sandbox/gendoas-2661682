<?php

/**
 * @file
 * Contains \Drupal\slogtb\Element\SlogtbElement.
 */

namespace Drupal\slogtb\Element;

use Drupal\slogtb\SlogTb;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Element;
use Drupal\slogtx\Interfaces\TxToolbarInterface;
use Drupal\slogtx\Interfaces\TxVocabularyInterface;
use Drupal\Core\Url;
use Drupal\toolbar\Element\ToolbarItem;
use Drupal\Component\Utility\Crypt;

/**
 * Provides a render element for the default Drupal toolbar.
 *
 * @RenderElement("slogtb")
 */
class SlogtbElement extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
        '#theme' => 'toolbar',
        '#pre_render' => [
            [get_class($this), 'preRenderToolbar'],
        ],
    ];
  }

  /**
   * Pre render callback for building a toolbar.
   * 
   * - The toolbar is built upon a toobar entity from slogtx
   * - The toolbar entity is a deep hierarchy with:
   *   - a set of vocabularies, which result in the toolbar tabs
   *   - a menu for each tab (vocab) up to three levels
   *   - with two levels between vocab and menu terms: target terms and root terms
   *   - of this two levels there is exactly one active at a time, so there is a 
   *     great amount of space for alternative menus for each toolbar/tab
   *
   * @param array $element
   *  Not yet ready renderable array with the slogtx toolbar as property
   * @return array
   * Ready renderable toolbar array
   */
  public static function preRenderToolbar($element) {
    $toolbar = $element['#toolbar'];
    $toolbar_id = $toolbar->id();
    $class_direction = $toolbar->isVertical() ? 'slogtb-vertical' : 'slogtb-horizontal';
    
    $libraries = ['slogtb/slogtb.core'];
    $handler_libs = SlogTb::getSlogtbHandler()->getLibraries();

    $element = [
        '#theme' => 'slogtb_toolbar',
        '#attached' => [
            'library' => array_merge($libraries, $handler_libs),
        ],
        // Metadata for the toolbar wrapping element.
        '#attributes' => [
            'id' => "slogtb-{$toolbar_id}",
            'class' => ["slogtb", $class_direction],
            'role' => 'group',
        ],
        // Metadata for the toolbar's bar element.
        '#bar' => [
            '#heading' => t('Slog toolbar items for %toolbar', ['%toolbar' => $toolbar_id]),
            '#attributes' => [
                'id' => "slogtb-{$toolbar_id}-bar",
                'class' => ['toolbar-bar', 'clearfix'],
                'role' => 'navigation',
            ],
        ],
            ] + $element;

    // Add toolbar tabs for each enabled and not empty vocabulary,
    // this is for current target term and current root term.
    $toolbar_tabs = self::buildToolbarTabItems($toolbar);
    if (!empty($toolbar_tabs)) {
      if (isset($toolbar_tabs['toolbar_icon'])) {
        // provide toolbar with an icon
        $element['#attributes']['class'][] = 'toolbar-icon-' . $toolbar_tabs['toolbar_icon'];
        unset($toolbar_tabs['toolbar_icon']);
      }

      // Allow altering toolbar tabs (hook_slogtb).
      \Drupal::moduleHandler()->alter('slogtb', $toolbar_tabs);
      // Sort the toolbar tabs by weight.
      uasort($toolbar_tabs, ['\Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);
      // add toolbar tabs to the toolbar element
      $element = array_merge($element, $toolbar_tabs);
      // add element wrapper attributes
      $element['#wrapper_attributes'] = [
          'class' => ['toolbar-tab', 'toolbar-tab-' . $toolbar_id],
      ];
    }

    return $element;
  }

  /**
   * Build the toolbar tabs upon each toolbar's vocabulary.
   * 
   * @param TxToolbarInterface $toolbar
   * @return array
   *  Each item a renderable array for the tab's menu
   */
  protected static function buildToolbarTabItems(TxToolbarInterface $toolbar) {
    $build = [];

    if ($toolbar->id() === SlogTx::TOOLBAR_ID_SYS) {
      $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, SlogTx::TBTAB_SYSID_DUMMY]);
      $vocabularies = [$vid => SlogTx::getVocabulary($vid)];
    }
    else {
      $vocabularies = $toolbar->getVocabularies();
    }
    $request = \Drupal::request();
    $slotx_voc = $request->get('slogtx_voc', false);
    $isOnRefreshTbmenu = ($request->get('_route', false) === 'slogtb.refresh.tbmenu.top');

    // for this route build for asked vocabulary only (see class RefreshMenuTop)
    if ($request->get('_route') == 'slogtb.refresh.tbmenu.top') {
      // notice: root term is set in class RefreshMenuTop
      $vocab = $request->get('slogtx_voc');
      if ($vocab instanceof TxVocabularyInterface) {
        $vocabularies = array_intersect_key($vocabularies, [$vocab->id() => $vocab]);
      }
    }

    //
    if ($isOnRefreshTbmenu && isset($vocabularies[$slotx_voc])) {
      $vocabularies = [$slotx_voc => $vocabularies[$slotx_voc]];
    }

    // there is a toolbar tab for each enabled and not empty vocabulary,
    // but also for empty role dependent vocabularies.
    $vocab_first = false;
    foreach ($vocabularies as $vid => $vocabulary) {
      if (!$vocabulary->isEnabled() || !$vocabulary->currentHasMenuitems()) {
        continue;
      }

      $vocab_first = $vocab_first ?: $vocabulary;
      $item = self::buildToolbarTabItem($vocabulary, $isOnRefreshTbmenu);
      if (!empty($item)) {
        $build[$vid] = $item;
      }
    }

    // return toolbar icon of first vocabulary
    if (!empty($build) && $vocab_first) {
      $build['toolbar_icon'] = $vocab_first->get('icon_id');
    }

    return $build;
  }

  /**
   * Built a menu for a toolbar tab upon the vocabulary.
   * 
   * @param TxVocabularyInterface $vocabulary
   * @return array
   *  A renderable array for the tab's menu
   */
  protected static function buildToolbarTabItem(TxVocabularyInterface $vocabulary, $isOnRefreshTbmenu = FALSE) {
    $vid = $vocabulary->id();
    $root_term = $vocabulary->getCurrentRootTerm();
    if (!$target_term = ($root_term ? $root_term->getTargetTerm() : FALSE)) {
      $args = ['%vocab' => $vocabulary->id()];
      SlogTb::logger()->error(t('Current target term not found in %vocab vocabulary.', $args));
      // abort - shouldn't happen
      return [];
    }

    list($toolbar_id, $toolbartab) = SlogTx::getVocabularyIdParts($vid);
    $targetterm_name = $target_term->label();
    $rootterm_name = $root_term->label();

    $tree_service = SlogTb::getMenuLinkTreeService();
    $tree = $tree_service->getTopMenuTree($vocabulary, $rootterm_name);
    $menu_data = $tree_service->build($tree);
    $hasMenuData = isset($menu_data['#items']) && !empty(Element::children($menu_data['#items']));


    if ($hasMenuData || $vocabulary->isRoleTargetEntity()) {
      $vocabulary->setCurrentTargetEntity();
      $toolbar = $vocabulary->getToolbar();
      $menu_class = "toolbar-menu-$vid";
      $icon_id = $vocabulary->get('icon_id');
      $hasSubtrees = self::hasSubtrees($tree);
      $rootterm_id = $root_term->id();
      $url = Url::fromRoute('slogtb.on.menu_tab_select', ['rootterm_id' => $rootterm_id]);

      // build tab data
      $tab = [
          '#type' => 'link',
          '#title' => $vocabulary->getTbtabLabel(),
          '#url' => $url, // for mouse right button -> new tab / new window
          '#attributes' => [
              'title' => $vocabulary->getTbtabDescription(),
              'class' => ['toolbar-icon', "toolbar-icon-{$icon_id}"],
              // presence of the attribute is necessary.
              // see toolbar_toolbar() in toolbar module
              'data-drupal-subtrees' => '',
              // more data attributes from the current hierarchy
              'toolbar' => $toolbar_id,
              'toolbartab' => $toolbartab,
              'targetterm' => $targetterm_name,
              'rootterm' => $rootterm_name,
              'roottermid' => $rootterm_id,
          ],
      ];

      // build toolbar data and make them global available
      // see also XtsiTbSubmenuContent::getResult()
      $menu_hashes = [];
      $cid = "Slogtb::menuHashes::$vid";
      if ($isOnRefreshTbmenu && $cache = SlogXt::cache()->get($cid)) {
        $menu_hashes = $cache->data;
      }

      if (empty($menu_hashes)) {
        foreach (array_keys($menu_data['#items']) as $tid) {
          $tid_hashes = SlogTb::getSlogtbHandler()->getTbMenuContentHashes($tid);
          $menu_hashes[$tid] = Crypt::hashBase64(serialize($tid_hashes));
        }
        SlogXt::cache()->set($cid, $menu_hashes);
      }

      $subtreesHash = $hasSubtrees ? $tree_service->getSubtreesHash($vid, $rootterm_name) : FALSE;
      $toolbar_data = [
          'tbBundleLabel' => $toolbar->label(),
          'isVertical' => (boolean) $toolbar->isVertical(),
          'menus' => [$menu_class => $toolbartab],
          'menuHashes' => $menu_hashes,
          'menutabs' => [$toolbartab => [
                  'targetEntity' => $vocabulary->getTargetEnityTypeId(),
                  'targetTermName' => $targetterm_name,
                  'rootTermName' => $rootterm_name,
                  'rootTermId' => $root_term->id(),
//todo::now - hasSubtrees
//                  'hasSubtrees' => $hasSubtrees,
                  'subtreesHash' => $subtreesHash,
              ]],
      ];
      if ($vocabulary->isRoleTargetEntity()) {
        $toolbar_data['hasRoleTab'] = TRUE;
      }
      if ($hasMenuData) {
        $toolbar_data['hasMenus'] = TRUE;
        if ($hasSubtrees) {
          $toolbar_data['hasSubtrees'] = TRUE;
        }
      }
      // make global available, see XtsiTbSubmenuContent::getResult()
      SlogTx::mergeJsToolbarData($toolbar_id, $toolbar_data);

      // build tray data
      $tray = [
          '#heading' => $vocabulary->label() . ' ' . t('menu'),
          "slogtb_$vid" => [
              '#type' => 'container',
              '#attributes' => [
                  'class' => [$menu_class],
              ],
              "{$vid}_menu" => $menu_data,
          ],
      ];
      $tray['#attached']['drupalSettings']['slogtb'] = [
          'toolbars' => [$toolbar_id => $toolbar_data],
      ];

      // put collected data into render array
      $build = [
          '#type' => 'toolbar_item',
          '#id' => "slogtb-item-$vid",
          '#weight' => $vocabulary->get('weight'),
          'tab' => $tab,
          'tray' => $tray,
      ];
    }

    // do always cache
    $build['#cache'] = [
        'bin' => 'slogtb',
        'keys' => ['tbtab', $vid, 'top'],
    ];
    $root_term->addRootTermCaches();
    \Drupal::service('renderer')->addCacheableDependency($build, $root_term);

    return $build;
  }

  /**
   * Prepares variables for slog toolbars using core toolbar templates.
   * 
   * - Preserve element properties (tray, tab) for template_preprocess_toolbar(),
   * - Early rendering each item to prevent loosing element properties.
   * 
   * @see slogtb_preprocess_slogtb()
   * @see template_preprocess_toolbar()
   * 
   * @param array $variables
   *   An associative array containing:
   *   - element: An associative array containing the properties and children of
   *     the tray. Properties used: #children, #attributes and #bar.
   */
  public static function preProcessToolbar(&$variables) {
    $element = &$variables['element'];
    foreach (Element::children($element) as $key) {
      $render_element = $element[$key];
      $render_element = ToolbarItem::preRenderToolbarItem($render_element);
      \Drupal::service('renderer')->render($render_element);
      $element[$key] = $render_element + $element[$key];
    }
    template_preprocess_toolbar($variables);
  }

  /**
   * Determine if there are subtrees
   * 
   * @param array $tree
   * @return boolean
   *  TRUE if any of the items has children
   */
  protected static function hasSubtrees(array $tree) {
    foreach ($tree as $data) {
      if ($data->hasChildren) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
