<?php

/**
 * @file
 * Contains \Drupal\slogtb\Form\TbPreparableSelectForm.
 */

namespace Drupal\slogtb\Form;

use Drupal\slogtb\SlogTb;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\XtUserData;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class TbPreparableSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogtb_tbpreparable_select_form';
  }

  protected function getPath($id) {
    if (empty($this->path_info)) {
      $request = \Drupal::request();
      $this->path_info = urldecode($request->getPathInfo());
      $this->replace_key = $request->get('pathReplaceKey', '{base_entity_id}');
    }

    return str_replace($this->replace_key, $id, $this->path_info);
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $plugin_id = $request->get('action_plugin_id');
    $action_plugin = SlogXt::pluginManager('edit')->createInstance($plugin_id);
    $action_perms = $action_plugin->getPermissions();
    
    $options = [];
    $toolbars = SlogTb::getToolbarsPreparable();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      if (SlogXt::hasPermToolbar($toolbar_id, $action_perms)) {
        if (SlogTx::getTbObjectToPrepare($toolbar->id())) {
          $description = $toolbar->getDescription();
          if ($toolbar_id === 'user') {
            //todo::current::permissions::user - constraints
            // not here, show page with permissions
            $user = \Drupal::currentUser();
            $description = $user->getAccountName();
          }
          else if ($toolbar_id === 'role') {
            $default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
            $description = $default_role->label();
          }

          $options[] = [
            'entityid' => $toolbar_id,
            'liTitle' => $toolbar->label(),
            'liDescription' => $description,
            'path' => $this->getPath($toolbar_id),
          ];
        }
      }
    }

    return $options;
  }

}
