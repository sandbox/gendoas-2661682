<?php

/**
 * @file
 * Contains \Drupal\slogtb\Routing\SlogtbRoutes.
 */

namespace Drupal\slogtb\Routing;

use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class SlogtbRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $basePath = trim(SlogXt::getBasePath('slogtb'), '/');
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('slogtb'), '/');
    $routes = [];
    $raw_data = [];

    $raw_data += $this->_rawAjaxBase($baseAjaxPath);
    $raw_data += $this->_rawAjaxRefresh($baseAjaxPath);
    $raw_data += $this->_rawAjaxEditMain($baseAjaxPath);

    $defaults = [
//        'requirements' => ['_access' => 'TRUE'],
      'requirements' => ['_permission' => 'access content'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["slogtb.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  private function _rawAjaxBase($baseAjaxPath) {
    $controllerPath = '\Drupal\slogtb\Controller';
    $raw_data = [
      'on.load_subtrees' => [
        'path' => "/$baseAjaxPath/subtrees/{toolbar}/{toolbartab}/{targetterm}/{rootterm}/{hash}",
        'defaults' => [
          '_controller' => "{$controllerPath}\SlogtbSubtreesController::ajaxResponse",
        ],
        'options' => self::$optAjaxBase,
        'requirements' => [
          '_access' => 'TRUE',
        ],
      ],
      'on.menu_item_select' => [
        'path' => "/$baseAjaxPath/menutid/{tbmenu_tid}",
        'defaults' => [
          '_controller' => "{$controllerPath}\OnTbMenuItemSelect::ajaxResponse",
        ],
        'options' => self::$optAjaxBase,
        'requirements' => [
          '_access' => 'TRUE',
        ],
      ],
      'on.menu_tab_select' => [
        'path' => "/$baseAjaxPath/menutab/{rootterm_id}",
        'defaults' => [
          // is not really handled by this controller - allways redirected
          // see SlogitemEventSubscriber::onKernelRequest()
          // this is for mouse right button: newTab/newWindow
          '_controller' => '\Drupal\system\Controller\Http4xxController:on404',
          '_title' => 'Page not found',
        ],
        'options' => self::$optAjaxBase,
        'requirements' => [
          '_access' => 'TRUE',
        ],
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxRefresh($baseAjaxPath) {
    $controllerPath = '\Drupal\slogtb\Handler\refresh';
    $raw_data = [
      'refresh.tbmenu.top' => [
        'path' => "/$baseAjaxPath/refresh/tbmenu/{slogtx_voc}/{slogtx_rt}",
        'defaults' => [
          '_controller' => "{$controllerPath}\RefreshMenuTop::ajaxResponse",
          'slogtx_rt' => 0,
        ],
        'options' => self::$optAjaxBase,
      ],
      'refresh.toolbar' => [
        'path' => "/$baseAjaxPath/refresh/toolbar/{slogtx_tb}",
        'defaults' => [
          '_controller' => "{$controllerPath}\RefreshToolbarBlock::ajaxResponse",
        ],
        //todo::top::top 'options' => ['_theme' => 'ajax_base_page']
        'options' => self::$optAjaxBase,
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxEditMain($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $next_entity_id = '{next_entity_id}';
    $controllerPath = '\Drupal\slogtb\Handler\edit\xtMain';

    $raw_data = [
      'edit.main.tbtab.new' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbtab/$base_entity_id/new",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbTabNewController::getContentResult",
        ],
      ],
      'edit.main.tbtab.edit' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbtab/$base_entity_id/edit",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbTabEditController::getContentResult",
        ],
      ],
      'edit.main.tbmenu.new' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbmenu/$base_entity_id/new",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbMenuNewController::getContentResult",
        ],
      ],
      'edit.main.tbmenu.edit' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbmenu/$base_entity_id/edit",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbMenuEditController::getContentResult",
        ],
      ],
      'edit.main.tbmenu.move' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbmenu/$base_entity_id/move/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbMenuMoveController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.main.tbmenu.rearrange' => [
        'path' => "/$baseAjaxPath/edit_xtmain/tbmenu/$base_entity_id/rearrange/{next_entity_id}",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbMenuRearrangeController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
    ];

    return $raw_data;
  }

}
