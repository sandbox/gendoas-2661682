<?php

/**
 * @file
 * Contains \Drupal\slogtb\Access\StbXtRolePermissions.
 */

namespace Drupal\slogtb\Access;

/**
 * Provides permissions for Xt-Roles.
 */
class StbXtRolePermissions {

  /**
   * Returns an array of Xtsi Xt-Role permissions.
   *
   * @return array
   *   The Xt-Role permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getXtRolePermissions() {
    $perms = [
      'admin sxtrole-menu' => [
        'title' => 'Administer Xt-Role menu',
        'description' => 'Permission for each of the Xt-Role menu actions (new, edit, move, rearange).',
        'warning' => 'Warning: This can be rather technical and should only be granted to power users.',
      ],
      'new sxtrole-menu' => [
        'title' => 'Add new Xt-Role menu item',
        'description' => 'Permission for adding new Xt-Role menu items',
      ],
      'edit sxtrole-menu' => [
        'title' => 'Edit Xt-Role menu item',
        'description' => 'Permission for editing Xt-Role menu items',
      ],
      'move sxtrole-menu' => [
        'title' => 'Move Xt-Role menu',
        'description' => 'Permission for moving Xt-Role menu items',
      ],
      'rearrange sxtrole-menu' => [
        'title' => 'Rearrange Xt-Role menu',
        'description' => 'Permission for rearranging Xt-Role menu items',
      ],
      'new sxtrole-tbtab' => [
        'title' => 'Add new Xt-Role top menu item',
        'description' => 'Permission for adding new Xt-Role top menu items',
      ],
      'edit sxtrole-tbtab' => [
        'title' => 'Edit Xt-Role top menu item',
        'description' => 'Permission for editing Xt-Role top menu items',
      ],
    ];
    
    return $perms;
  }

}
