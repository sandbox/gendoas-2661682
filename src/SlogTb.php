<?php

/**
 * @file
 * Contains \Drupal\slogtb\SlogTb.
 */

namespace Drupal\slogtb;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\Event\SlogtbEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;
use Drupal\slogtb\Handler\SlogtbHandlerInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\block\Entity\Block;
use Drupal\Core\Cache\Cache;

/**
 * Static helper functions and static service container wrapper for slogtb module.
 */
class SlogTb {

  /**
   * Maximal depth of vocabulary items.
   */
  const ADMIN_TB_CATEGORY = 'SlogTb';
  const ADMIN_TB_MENU = 'SlogTb menu';
  const ADMIN_TB_CONTENT = 'SlogTb content';
  const ADMIN_TB_SEPARATOR = ': ';
  const STB_ID_TOOLBAR = 'stb_toolbar';
  const STB_ID_CONTENT = 'stb_content';

  protected static $slogtb_handler;
  
  protected static $blockIdByToolbarId = NULL;
  protected static $blockByBlockId = NULL;
  protected static $blockByPluginId = NULL;

  /**
   * Cache retrieved logger objects for channels
   *
   * @var array of \Psr\Log\LoggerInterface objects
   */
  protected static $cacheLogger = [];
  protected static $cacheToolbarsPreparable = NULL;
  protected static $hasEditableToolbar = NULL;

  /**
   * Determine the handler for handling slog toolbar events.
   * 
   * Concerns the events slog toolbar does not handle itself,
   * e.g. a menu item has been clicked.
   * 
   * @return Drupal\slogtb\Handler\SlogtbHandlerInterface
   * @throws \InvalidArgumentException
   */
  public static function getSlogtbHandler() {
    if (!isset(self::$slogtb_handler)) {
      // ensure theme is initialized
      $theme = \Drupal::theme()->getActiveTheme()->getName();

      // We have to get one handler.
      // see \Drupal\slogtb\Event\SlogtbEventSubscriber::onRegisterSlogtbHandler()
      // If no other module handles event, this module handles it by dummy handler.
      // see \Drupal\slogtb\Handler\SlogtbDummyHandler
      $event = \Drupal::service('event_dispatcher')->dispatch(SlogtbEvents::REGISTER_SLOGTB_HANDLER);
      $handler_class = $event->slogtbHandlerClass;

      // create an instance of the handler class
      $class_resolver = \Drupal::service('class_resolver');
      $handler = $class_resolver->getInstanceFromDefinition($handler_class);
      if (!is_object($handler) || !($handler instanceof SlogtbHandlerInterface)) {
        throw new \RuntimeException("The Handler '{$handler_class}' does not implement SlogtbHandlerInterface.");
      }

      self::$slogtb_handler = $handler;
    }

    return self::$slogtb_handler;
  }

  /**
   * Return the function on client side which resolves the node id for a path.
   */
  public static function getJsDataResolvePathNode($key) {
    return self::getSlogtbHandler()->getJsDataResolvePathNode($key);
  }

  /**
   * Return the function on client side which resolves the node id for a path.
   */
  public static function getJsRegionData() {
    $result = [];
    foreach (SlogXt::getVisibleBlocksPerRegion() as $region => $blocks) {
      foreach ($blocks as $block) {
        $parts = explode(PluginBase::DERIVATIVE_SEPARATOR, $block->getPluginId());
        if (count($parts) === 2) {
          list($base_id, $toolbar) = $parts;
          if ($base_id === self::STB_ID_TOOLBAR) {
            $result[$toolbar] = $region;
          }
        }
      }
    }
    return $result;
  }

  /**
   * Return prepare tag for each preparable toolbar (for client side).
   * 
   * @return array
   */
  public static function getJsPreparableToolbars() {
    $result = [];
    $toolbars = self::getToolbarsPreparable();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $result[$toolbar_id] = 'prepare' . SlogXt::XTURL_DELIMITER . $toolbar_id;
    }
    return $result;
  }

  /**
   * Return a logger object.
   * 
   * @return \Psr\Log\LoggerInterface, default channel 'slogtb'
   */
  public static function logger($channel = 'slogtb') {
    if (!isset(self::$cacheLogger[$channel])) {
      self::$cacheLogger[$channel] = \Drupal::logger($channel);
    }
    return self::$cacheLogger[$channel];
  }

  /**
   * Return a toolbar object.
   * 
   * Wrapper for \Drupal\slogtx\SlogTx::getToolbar()
   * 
   * @param string $toolbar_id
   * @return Drupal\slogtx\Interfaces\TxToolbarInterface
   *   The toolbar object associated with the specified toolbar id.
   */
  public static function getToolbar($toolbar_id) {
    return SlogTx::getToolbar($toolbar_id);
  }

  /**
   * Return all enabled toolbars.
   * 
   * Wrapper for \Drupal\slogtx\SlogTx::getToolbars()
   * 
   * @return array of toolbar objects.
   *   \Drupal\slogtx\Interfaces\TxToolbarInterface
   */
  public static function getToolbars() {
    return SlogTx::getToolbars();
  }

  /**
   * Return all toolbars that are visible as menu, but exclude underscored.
   * 
   * @param string $toolbar_id
   * @return boolean
   */
  public static function isToolbarVisible($toolbar_id) {
    $toolbars = self::getToolbarsVisible();
    return !empty($toolbars[$toolbar_id]);
  }

  /**
   * Whether toolbar is visible as menu (exclude underscored).
   * 
   * - For visability: enabled and has menu items.
   * - Underscored: i.g. _sys, _node
   * 
   * @return array of toolbars
   */
  public static function getToolbarsVisible() {
    return SlogTx::getToolbarsVisible();
  }

  /**
   * Whether toolbar has a preparable vocabulary (top menu item).
   * 
   * Required for 'preparable'
   *  - toolbar is enabled
   *  - there ist a prepareble vocabulary
   *  - vocabulary is enabled
   *  - current root terms has no menu terms (->create)
   *  - OR current target term is disabled (->activate)
   * 
   * @param string $toolbar_id
   *  Maybe toolbar as object or only toolbar id
   * @return boolean
   */
  public static function isToolbarPrepareable($toolbar_id) {
    $toolbars = self::getToolbarsPreparable();
    return !empty($toolbars[$toolbar_id]);
  }

  public static function getToolbarsPreparable() {
    if (!isset(self::$cacheToolbarsPreparable)) {
      self::$cacheToolbarsPreparable = [];
      $toolbars = SlogTx::getToolbarsSorted();
      foreach ($toolbars as $toolbar_id => $toolbar) {
        if (!$toolbar->isUnderscoreToolbar()) {
          $block = self::getSlogtbMenuBlock($toolbar_id);
          $enforce_teid = $toolbar->getEnforceTargetEntity();
          if (!empty($block) && !empty($enforce_teid)) {
            if (!$preparable = SlogTx::hasTbObjectToPrepare($toolbar_id)) {
              // ensure definitions and try again
              SlogTx::pluginManager('rtget_tb_menu')->getDefinitions();
              $preparable = SlogTx::hasTbObjectToPrepare($toolbar_id);
            }
            if ($preparable) {
              self::$cacheToolbarsPreparable[$toolbar_id] = $toolbar;
            }
          }
        }
      }
    }

    return self::$cacheToolbarsPreparable;
  }

  public static function hasGlobalTbAdmin($account = NULL) {
    $account = $account ?: \Drupal::currentUser();
    if ($account->hasPermission('administer slog taxonomy')) {
      return TRUE;
    }
    else {
      foreach (SlogTx::getToolbarsVisibleGlobal() as $toolbar_id => $toolbar) {
        if ($account->hasPermission("administer toolbar $toolbar_id")) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  public static function getTbmenuEditOtherPerm($plugin_id) {
    $result = [];
    $data = ['plugin_id' => $plugin_id, 'result' => &$result];
    $event = new SlogxtGenericEvent($data);
    \Drupal::service('event_dispatcher')
            ->dispatch(SlogtbEvents::SLOGTB_HAS_TBMENU_EDIT_PERM, $event);
    return $result;
  }

  public static function hasEditableToolbar() {
    if (!isset(self::$hasEditableToolbar)) {
      self::$hasEditableToolbar = ((self::isToolbarVisible('user') && self::isToolbarPrepareable('user'))  //
              || self::hasGlobalTbAdmin()  //
              || self::hasOneOfRolePerms());
    }
    return self::$hasEditableToolbar;
  }

  private static function hasOneOfRolePerms() {
    $test_perms = [
        'edit sxtrole-menu',
        'new sxtrole-menu',
        'move sxtrole-menu',
        'rearrange sxtrole-menu',
        'edit sxtrole-tbtab',
        'new sxtrole-tbtab',
    ];
    foreach ($test_perms as $test_perm) {
      if (SlogXt::hasPermToolbar('role', ['role' => $test_perm])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public static function hasTbmenuEditOtherPerm($plugin_id) {
    return !empty(self::getTbmenuEditOtherPerm($plugin_id));
  }

  public static function hasOtherTbPerm($plugin_id) {
    return !empty(self::hasTbmenuEditOtherPerm($plugin_id));
  }

  public static function getMenuLinkTreeService() {
    return \Drupal::service('slogtb.menu.link_tree');
  }

  public static function getTargetSelectFormArg($path_target) {
    $plugin_id = "slogxt_xtmain_{$path_target}";
    $definition = SlogXt::pluginManager('action')->getDefinition($plugin_id);
    if ($definition && !empty($definition['target_select_form'])) {
      return $definition['target_select_form'];
    }
    return '\Drupal\slogxt\Form\dummy\DummyCheckboxesForm';
  }

  public static function getXtSysRootTermFormArg($base_entity_id) {
    $result = ['doTbMenuSelect' => TRUE, 'type' => 'unknown'];
    list($target, $targetentity_id, $entity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
    $plugin_id = "slogxt_xtmain_{$target}";
    $plugin_manager = SlogXt::pluginManager('action');
    if (!$definition = $plugin_manager->getDefinition($plugin_id)) {
      $message = t('Definition not found: @pid.', ['@pid' => $plugin_id]);
      throw new \LogicException($message);
    }

    if (!$action_plugin = $plugin_manager->createInstance($plugin_id)) {
      $message = t('Plugin not created: @pid.', ['@pid' => $plugin_id]);
      throw new \LogicException($message);
    }

    if (!$action_plugin->hasXtSysRootTerm()) {
      $message = t('Unvalid target plugin: @pid.', ['@pid' => $plugin_id]);
      throw new \LogicException($message);
    }

    if (!empty($targetentity_id)) {
      $result = $action_plugin->getXtSysRootTerm($base_entity_id) + $result;
    }

    return $result;
  }

  public static function getBlockByPluginId($plugin_id) {
    if (empty(self::$blockByPluginId)) {
      self::populateToolbarBlockData();
    }

    return self::$blockByPluginId[$plugin_id];
  }

  public static function resetToolbarBlockData() {
    self::$blockByPluginId = NULL;
    self::$blockByBlockId = NULL;
    self::$blockIdByToolbarId = NULL;
  }

  public static function populateToolbarBlockData() {
    $separator = PluginBase::DERIVATIVE_SEPARATOR;
    self::$blockByPluginId = $blockByPluginId = [];
    self::$blockByBlockId = $blockByBlockId = [];
    self::$blockIdByToolbarId = $blockIdByToolbarId = [
        self::STB_ID_TOOLBAR => [],
        self::STB_ID_CONTENT => [],
    ];
    $allowed_base_ids = array_keys($blockIdByToolbarId);

    $found_toolbar_ids = [];
    foreach (SlogXt::getVisibleBlocksPerRegion() as $region => $blocks) {
      foreach ($blocks as $key => $block) {
        $plugin_id = $block->getPluginId();
        $block_id = $block->id();
        list($base_id, $toolbar_id) = explode($separator, $plugin_id . $separator);
        if (!empty($toolbar_id)) {
          $found_toolbar_ids[] = "$base_id.$toolbar_id";
        }
        $blockByPluginId[$plugin_id] = $block;
        $blockByBlockId[$block_id] = $block;
        if (in_array($base_id, $allowed_base_ids) && !empty($toolbar_id)) {
          $blockIdByToolbarId[$base_id][$toolbar_id] = $block_id;
        }
      }
    }

    $required = [
        self::STB_ID_TOOLBAR . '.' . SlogTx::TOOLBAR_ID_SYS,
        self::STB_ID_TOOLBAR . '.' . SlogTx::TOOLBAR_ID_NODE_SUBSYS,
        self::STB_ID_CONTENT . '.' . SlogTx::TOOLBAR_ID_SYS,
        self::STB_ID_CONTENT . '.' . SlogTx::TOOLBAR_ID_NODE_SUBSYS,
    ];
    $required_found = array_intersect($required, $found_toolbar_ids);
    $missing_ids = array_diff($required, $required_found);
    if (!empty($missing_ids)) {
      $msg = 'Toolbar blocks missing or not enabled: ' . implode(', ', $missing_ids);
      self::logger()->error($msg);
    }

    self::$blockByPluginId = $blockByPluginId;
    self::$blockByBlockId = $blockByBlockId;
    self::$blockIdByToolbarId = $blockIdByToolbarId;
  }

  public static function getSlogtbMenuBlock($toolbar_id) {
    return self::getSlogtbBlock(self::STB_ID_TOOLBAR, $toolbar_id);
  }

  public static function getSlogtbContentBlock($toolbar_id) {
    return self::getSlogtbBlock(self::STB_ID_CONTENT, $toolbar_id);
  }

  public static function getSlogtbBlock($base_id, $toolbar_id) {
    if (empty(self::$blockIdByToolbarId)) {
      self::populateToolbarBlockData();
    }

    $block_id = self::$blockIdByToolbarId[$base_id][$toolbar_id] ?? '';
    return (self::$blockByBlockId[$block_id] ?? FALSE);
  }

  public static function hasSlogtbBlockAny($base_id) {
    if (empty(self::$blockIdByToolbarId)) {
      self::populateToolbarBlockData();
    }

    return !empty(self::$blockIdByToolbarId[$base_id]);
  }

  public static function getTbHiddenMenuBlock($vocabulary) {
    $toolbar = $vocabulary->getToolbar();
    $toolbar_id = $toolbar ? $toolbar->id() : FALSE;
    $block = $toolbar_id ? self::getSlogtbMenuBlock($toolbar_id) : FALSE;
    return $block;
  }

  public static function getMenuBlockCacheTags($toolbar_id) {
    $block = self::getSlogtbMenuBlock($toolbar_id);
    $cache_tags = !empty($block) ? $block->getPlugin()->getCacheTags() : FALSE;
    return $cache_tags;
  }

  public static function invalidateBlockCache($cache_tags) {
    if (!empty($cache_tags)) {
      Cache::invalidateTags($cache_tags);
    }
  }

}
