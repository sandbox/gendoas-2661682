<?php

/**
 * @file
 * Contains \Drupal\slogtb\SlogtbMenuLinkTree.
 */

namespace Drupal\slogtb;

use Drupal\slogtb\SlogTb;
use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Entity\TxVocabulary;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\slogtx\Entity\RootTerm;

/**
 */
class SlogtbMenuLinkTree extends MenuLinkTree implements TrustedCallbackInterface {

  /**
   * Caches already built top menu trees
   *
   * @var array 
   */
  protected $topMenuTree = [];

  /**
   * {@inheritdoc}
   */
  public function build(array $tree) {
    $build = parent::build($tree);
    // use theme of toolbar module; remove cache tags added by parent
    $build['#theme'] = 'menu__toolbar';
    $build['#cache']['tags'] = [];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['doGetRenderedSubtrees'];
  }
  
  /**
   * Returns the rendered subtree of each top-level toolbar link.
   *
   * @return array
   *   An array with the following key-value pairs:
   *   - 'subtrees': the rendered subtrees
   *   - 'cacheability: the associated cacheability.
   */
  public function getRenderedSubtrees($vid, $rootterm_name) {
    if (!isset($this->rendered_subtrees[$vid])) {
      $this->rendered_subtrees[$vid] = ['#subtrees' => FALSE];
      $vocabulary = SlogTx::getVocabulary($vid);
      if ($vocabulary->isValidRootTermName($rootterm_name)) {
        $vocabulary->setCurrentRootTermName($rootterm_name);
        $root_term = $vocabulary->getCurrentRootTerm();
        $target_term = $root_term->getTargetTerm();
        $data = [
          '#pre_render' => [[get_class($this), 'doGetRenderedSubtrees']],
          '#cache' => [
            'bin' => 'slogtb',
            'keys' => ['tbtab', $vid, 'sub'],
          ],
          '#cache_properties' => ['#subtrees'],
          '#stbExtraData' => [$vid, $target_term->label(), $rootterm_name, $root_term->id()],
        ];

        $root_term->addRootTermCaches();
        $renderer = \Drupal::service('renderer');
        $renderer->addCacheableDependency($data, $root_term);

        $renderer->renderPlain($data);
        $this->rendered_subtrees[$vid] = $data['#subtrees'];
      } 
    }
    
    return $this->rendered_subtrees[$vid];
  }
  
  public function getSubtreesHash($vid, $rootterm_name) {
    $subtrees = $this->getRenderedSubtrees($vid, $rootterm_name);
    return $subtrees ? Crypt::hashBase64(serialize($subtrees)) : '';
  }

  public function getSubtreesMenuHashes($vid, $rootterm_name) {
    $subtrees = $this->getRenderedSubtrees($vid, $rootterm_name);
    return $subtrees && isset($subtrees['#menuhashes']) ? $subtrees['#menuhashes'] : FALSE;
  }

  /**
   * Pre render callback for rendering subtrees.
   * 
   * @see self::getRenderedSubtrees()
   * 
   * @param TxVocabulary $vocabulary
   *  The vocabulary for which to build subtrees
   * @param string $rootterm_name
   *  The term lbel of the root term
   * @return array
   *  Prepared render array from data parameter
   */
  public static function doGetRenderedSubtrees(array $elements) {
    list($vid, $target_term, $rootterm_name, $rootterm_id) = $elements['#stbExtraData'];
    unset($elements['#stbExtraData']);
    $vocabulary = SlogTx::getVocabulary($vid);

    $subtrees = [];
    if ($vocabulary->isValidRootTermName($rootterm_name)) {
      // here is no change of the current target term,
      // $rootterm_name has to fit to the current target term
      $vocabulary->setCurrentRootTermName($rootterm_name);
      $vid = $vocabulary->id();
      $root_term = $vocabulary->getCurrentRootTerm();
      $target_term = $root_term->getTargetTerm();
      $rootterm_id = $root_term->id();

      $menu_hashes = [];
      $renderer = \Drupal::service('renderer');
      $tree_service = SlogTb::getMenuLinkTreeService();
      $menutree = $tree_service->getTopMenuTree($vocabulary, $rootterm_name);
      foreach ($menutree as $menuitem) {
        $output = '';
        $menuitem_id = $menuitem->link->getPluginId();
        $subtree = $tree_service->getSubMenuTree($vocabulary, $rootterm_name, $menuitem_id);
        if (!empty($subtree)) {
          $subtree = $tree_service->build($subtree);
          $output = $renderer->renderPlain($subtree);
          
          $hash_tree = $vocabulary->getVocabularyMenuTree($menuitem_id);
          foreach ($hash_tree as $hash_item) {
            $tid = $hash_item->tid;
            $tid_hashes = SlogTb::getSlogtbHandler()->getTbMenuContentHashes($tid);
            $menu_hashes[$tid] = Crypt::hashBase64(serialize($tid_hashes));
          }
        }
        $subtrees[$menuitem_id] = $output;
      }
    }

    $renderer->addCacheableDependency($subtrees, $root_term);
    CacheableMetadata::createFromRenderArray($elements)
        ->merge(CacheableMetadata::createFromRenderArray($subtrees))
        ->applyTo($elements);

    $subtrees['#menuhashes'] = $menu_hashes;  // see SlogtbSubtreesController::responseData()
    $elements['#subtrees'] = $subtrees;
    return $elements;
  }

  /**
   * Helper function
   * 
   * @param integer $vid
   * @param string $rootterm_name
   * @param integer $parentTid
   * @param string $type 'TopMenu' or 'SubMenu'
   * @return MenuTreeParameters
   */
  private function getSlogMenuParams($vid, $rootterm_name, $parentTid, $type) {
    $parameters = new MenuTreeParameters();
    $parameters->addCondition('vid', $vid)
        ->addCondition('rootTermName', $rootterm_name)
        ->addCondition('parentTid', $parentTid)
        ->addCondition('type', $type);
    return $parameters;
  }

  /**
   * Return a sorted array with MenuLinkTreeElement objects for the sub menu.
   * 
   * @param Drupal\slogtx\Entity\TxVocabulary $vocabulary
   * @param string $rootterm_name
   * @param type $parentTid
   *  The ID of the top menu item for which to get sub menu tree
   * @return array
   *  Each item a Drupal\Core\Menu\MenuLinkTreeElement object
   */
  protected function getSubMenuTree(TxVocabulary $vocabulary, $rootterm_name, $parentTid) {
    // The array $parameters->conditions is used for cache cid
    // see SlogtbMenuTreeStorage::loadTreeData
    $vid = $vocabulary->id();
    $parameters = $this->getSlogMenuParams($vid, $rootterm_name, $parentTid, 'SubMenu');
    $tree = $this->load($vid, $parameters);

    $manipulators = [
      //todo::access::SlogtbMenuLinkTree.getSubMenuTree()
      //          ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    return $this->transform($tree, $manipulators);
  }

  //todo::fix::not used - search tbnode__nodeextension (deprecated)
  public function loadTopMenuItemsData($vid, $rootterm_name) {
    $data = [];
    if ($vocabulary = SlogTx::getVocabulary($vid)) {
      $parentTid = $vocabulary->getRootTermIdByName($rootterm_name);
      if ($parentTid >= 0) {
        $parameters = $this->getSlogMenuParams($vid, $rootterm_name, $parentTid, 'TopMenu');
        $data = $this->treeStorage->loadTreeData($vid, $parameters);
      }
    }

    return !empty($data['tree']) ? $data['tree'] : false;
  }

  /**
   * Load top items for a menu from vocabulary and root term.
   * 
   * - load from database (taxonomy terms)
   * - create instances of MenuLinkTreeElement class from the terms
   * - return a set of MenuLinkTreeElement objects
   * 
   * @param Drupal\slogtx\Entity\TxVocabulary $vocabulary
   * @param string $rootterm_name
   * @return array
   *  Each item a Drupal\Core\Menu\MenuLinkTreeElement object
   */
  public function loadTopMenuTree(TxVocabulary $vocabulary, $rootterm_name) {
    $vid = $vocabulary->id();
    $idx = "$vid:$rootterm_name";
    if (!isset($this->topMenuTree[$idx])) {
      $this->topMenuTree[$idx] = false;

      // we are forced to use MenuTreeParameters,
      // but we use conditions only
      $parentTid = $vocabulary->getRootTermIdByName($rootterm_name);
      if ($parentTid >= 0) {
        $parameters = $this->getSlogMenuParams($vid, $rootterm_name, $parentTid, 'TopMenu');
        $this->topMenuTree[$idx] = $this->load($vid, $parameters);
      }
    }

    return $this->topMenuTree[$idx];
  }

  /**
   * Return a sorted array with MenuLinkTreeElement objects for the top menu.
   * 
   * @param Drupal\slogtx\Entity\TxVocabulary $vocabulary
   * @param string $rootterm_name
   * @return array
   *  Each item a Drupal\Core\Menu\MenuLinkTreeElement object
   */
  public function getTopMenuTree(TxVocabulary $vocabulary, $rootterm_name) {
    $vid = $vocabulary->id();
    $idx = "menu:$vid:$rootterm_name";
    if (!isset($this->topMenuTree[$idx])) {
      $this->topMenuTree[$idx] = [];
      $tree = $this->loadTopMenuTree($vocabulary, $rootterm_name);
      if (!empty($tree)) {
        $manipulators = [
          //todo::access::SlogtbMenuLinkTree.getTopMenuTree()
          //          ['callable' => 'menu.default_tree_manipulators:checkAccess'],
          ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ];
        $this->topMenuTree[$idx] = $this->transform($tree, $manipulators);
      }
    }

    return $this->topMenuTree[$idx];
  }

  /**
   * 
   * @param Drupal\slogtx\Entity\RootTerm $root_term
   * @return boolean
   */
  public function hasMenuItems(RootTerm $root_term) {
    //todo::access::fix::hasMenuItems() - depends on user permissions???
    $menutree = $this->getMenuTreeByRootterm($root_term);
    return (!empty($menutree) && TRUE /* todo::access */);
  }

  /**
   * 
   * @param Drupal\slogtx\Entity\RootTerm $root_term
   * @return array
   */
  public function getMenuTreeByRootterm(RootTerm $root_term) {
    if ($root_term && $root_term->isRootTerm()) {
      $vocabulary = $root_term->getVocabulary();
      $targetterm = $root_term->getTargetTerm();
      $vocabulary->setCurrentTargetTermName($targetterm->label());
      return $this->getTopMenuTree($vocabulary, $root_term->label());
    }
    return FALSE;
  }

}
