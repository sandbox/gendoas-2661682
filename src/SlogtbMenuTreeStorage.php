<?php

/**
 * @file
 * Contains \Drupal\slogtb\SlogtbMenuTreeStorage.
 */

namespace Drupal\slogtb;

use Drupal\slogtb\SlogTb;
use Drupal\slogtx\SlogTx;
use Drupal\Core\Menu\MenuTreeStorage;
use Drupal\Core\Menu\MenuTreeStorageInterface;
use Drupal\slogtx\Entity\TxVocabulary;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 */
class SlogtbMenuTreeStorage extends MenuTreeStorage implements MenuTreeStorageInterface {

  /**
   * Overrides Drupal\Core\Menu\MenuTreeStorage::loadTreeData().
   * 
   *  - Parent function completely replaced.
   *  - Links are loaded upon slogtx vocabulary/terms.
   *  - Active trail is not supported.
   *  - Menus have up to three levels.
   * 
   * @param string $menu_name
   *  Corresponds to the vocabulary ID.
   *  The vocabulary ID is carried by parameters too.
   * @param MenuTreeParameters $parameters
   *  Parameters required for loading links.
   * @return array
   */
  public function loadTreeData($menu_name, MenuTreeParameters $parameters) {
    // load links upon slogtx vocabulary/terms.
    $links = $this->loadLinks($menu_name, $parameters);
    // activeTrail is not supported.
    // depth is always 2, we build menus with up to three levels.
    $data['tree'] = $this->doBuildTreeData($links, [], 2);
    $data['definitions'] = [];
    $data['route_names'] = $this->collectRoutesAndDefinitions($data['tree'], $data['definitions']);
      
    // return data without definitions
    unset($data['definitions']);
    return $data;
  }

  /**
   * Overrides Drupal\Core\Menu\MenuTreeStorage::loadLinks().
   * 
   *  - Parent function completely replaced.
   *  - Links are loaded/build from slogtx terms.
   * 
   * @param type $menu_name
   *  Corresponds to the vocabulary ID, but is not used.
   *  Instead $params['vid'] is used for retrieving vocabulary.
   * @param \Drupal\Core\Menu\MenuTreeParameters $parameters
   *  $parameters->conditions used only.
   * @return array
   */
  protected function loadLinks($menu_name, MenuTreeParameters $parameters) {
    $links = [];

    $params = $parameters->conditions;
    $parentTid = $params['parentTid'];
    $vocabulary = SlogTx::getVocabulary($params['vid']);
    if ($params['type'] == 'TopMenu') {
      $tree = $vocabulary->getRootTermTree($params['rootTermName'], 1);
      $links = $this->buildLinksRecursive($vocabulary, $tree, [$parentTid], 1);
    } else {  // $params['type'] == 'SubMenu'
      $tree = $vocabulary->getVocabularyMenuTree($parentTid, 1); //@todo
      $links = $this->buildLinksRecursive($vocabulary, $tree, [$parentTid], 4);
    }

    return $links;
  }

  /**
   * 
   * @param TxVocabulary $vocabulary
   * @param array $tree
   * @param array $parents
   * @param type $depth
   * @return type
   */
  protected function buildLinksRecursive(TxVocabulary $vocabulary, array $tree, array $parents, $depth) {
    $route_name =  SlogTb::getSlogtbHandler()->getRouteName();
    $links = [];

    if (!empty($tree) && is_array($parents) && !empty($parents)) {
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      foreach ($tree as $item) {
        $linkId = $item->tid;
        $children = $vocabulary->getVocabularyMenuTree($linkId, 1);
        $has_children = !empty($children) ? 1 : 0;
        $item_parents = $parents;
        $item_parents[] = $linkId;
        $plid = $parents[count($parents) - 1];

        $link = [
            'class' => 'Drupal\Core\Menu\MenuLinkDefault',
            'form_class' => 'Drupal\Core\Menu\Form\MenuLinkDefaultForm',
            'depth' => count($item_parents),
            'has_children' => $has_children,
            'enabled' => TRUE,
            'hidden' => FALSE,
            'id' => $linkId,
            'menu_name' => $item->vid,
            'metadata' => [],
            'mlid' => $linkId,
            'options' => [
                'attributes' => [
                    'id' => "slogtb-link-" . $linkId,
                    'entityid' => $linkId,
                    'class' => ['toolbar-icon', 'slogtb-link-item'],
                    'title' => $item->description__value,
                ],
            ],
            // assign parents later: p1, p2, ...
            'parent' => $plid,
            'provider' => $vocabulary->get('provider'),
            'route_name' => $route_name,
            'route_parameters' => [
                'tbmenu_tid' => $linkId,
            ],
            'title' => $item->name,
            'title_context' => '',
            'title_arguments' => [],
            'description' => $item->description__value,
            'url' => '',
            'weight' => $item->weight,
        ];

        // assign parents
        foreach ($item_parents as $key => $parent) {
          $idx = $key + 1;
          $link["p$idx"] = $parent;
        }
        
        // add this link
        $links[$linkId] = $link;

        if ($depth > 2 && $has_children) {
          // add sub links 
          $links += $this->buildLinksRecursive($vocabulary, $children, $item_parents, $depth - 1);
        }
      }
    }

    return $links;
  }

  /**
   * Overrides MenuTreeStorage::serializedFields
   *
   * @return array
   *   An empty array, since there is no serialized field.
   */
  protected function serializedFields() {
    return [];
  }

}
