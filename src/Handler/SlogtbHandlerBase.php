<?php

/**
 * @file
 * Definition of \Drupal\slogtb\Handler\SlogtbHandlerBase.
 */

namespace Drupal\slogtb\Handler;

use Drupal\slogtb\SlogTb;
use Drupal\slogxt\SlogXt;

abstract class SlogtbHandlerBase implements SlogtbHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibrariesAutoload() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName($route_name_ext = '.on.menu_item_select') {
    return $this->getProvider() . $route_name_ext;
  }

  /**
   * {@inheritdoc}
   */
  public function getPathPrefix() {
    // without leading slash and with trailing slash !!!
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath($this->getProvider()), '/');
    return "$baseAjaxPath/menutid/";
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTarget() {
    $selector = '';
    if ($this->hasSlogtbContentDefaultBlock()) {
      $selector = '.block-slogtb.block-slogtb-content-default';
    }
    if (empty($selector)) {
      $theme = \Drupal::theme()->getActiveTheme()->getName();
      switch ($theme) {
        case 'sjqlout':
          //lout-c-center
          // do not serve a valid selector, is handled by sjqlout (JS)
          $selector = '#lout-c-center .region-center';
          break;
//        case 'bartik':
//          $selector = 'main#content .region-content';
//          break;
        default:
          $selector = 'main#content .region-content';
          break;
      }
    }

    return $selector;
  }

  /**
   * {@inheritdoc}
   */
  public function getTbMenuContentItems($menu_tid) {
    return [];
  }

  public function getTbMenuContentHashes($menu_tid) {
    return [];
  }

  /**
   * Whether there is a visible block with id=slogtb_content_default.
   * 
   * @return boolean
   *  TRUE if placed in any region, FALSE otherwise.
   */
  protected function hasSlogtbContentDefaultBlock() {
    foreach (SlogXt::getVisibleBlocksPerRegion() as $region => $blocks) {
      foreach ($blocks as $key => $block) {
        if ($block->getPluginId() == 'slogtb_content_default') {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsDataResolvePathNode($key) {
    return SlogTb::getJsDataResolvePathNode($key);
  }

}
