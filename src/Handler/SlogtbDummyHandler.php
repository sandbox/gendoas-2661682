<?php

/**
 * @file
 * Definition of \Drupal\slogtb\Handler\SlogtbDummyHandler.
 */

namespace Drupal\slogtb\Handler;

class SlogtbDummyHandler extends SlogtbHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'slogtb';
  }
  
}
