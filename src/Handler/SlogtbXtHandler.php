<?php

/**
 * @file
 * Definition of \Drupal\slogtb\Handler\SlogtbXtHandler.
 */

namespace Drupal\slogtb\Handler;

use Drupal\slogtb\SlogTb;
use Drupal\slogxt\Handler\SlogxtHandlerBase;

class SlogtbXtHandler extends SlogxtHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'slogtb';
  }

  /**
   * {@inheritdoc}
   */
  public function getSubHandler() {
    return SlogTb::getSlogtbHandler();
  }
  
  /**
   * {@inheritdoc}
   */
  public function getJsDataResolvePathNode($key) {
    return SlogTb::getJsDataResolvePathNode($key);
  }

}
