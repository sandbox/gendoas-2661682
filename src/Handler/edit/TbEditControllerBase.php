<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\TbEditControllerBase.
 */

namespace Drupal\slogtb\Handler\edit;

use Drupal\slogtb\SlogTb;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
abstract class TbEditControllerBase extends XtEditControllerBase {

  public function getSlogtbHandler() {
    return SlogTb::getSlogtbHandler();
  }

}
