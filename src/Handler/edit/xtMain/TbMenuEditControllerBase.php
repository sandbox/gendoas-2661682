<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbMenuEditControllerBase.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtb\Handler\edit\TbEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
abstract class TbMenuEditControllerBase extends TbEditControllerBase {

  protected $menuTerm;
  protected $opSave = false;
  protected $doTbMenuSelect = false;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      $slogxtData['runCommand'] = 'radios';
    }
    else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * This is for both, edit and add new menu item.
   * 
   * @param type $form
   * @param FormStateInterface $form_state
   */
  protected function preRenderFormLast(&$form, FormStateInterface $form_state) {
    if (isset($form['name']) && is_array($form['name']) && $form['name']['#description_display'] !== 'invisible') {
      $element = &$form['name']['widget'][0];
      $element['#description'] = t('The label of the menu item.');
      $element['value']['#description'] = $element['#description'];
    }
    if (isset($form['description']) && is_array($form['description'])) {
      $element = &$form['description']['widget'][0];
      $element['#description'] = t('A description of the menu item.');
    }
    if (isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      } else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    } elseif (empty($this->doTbMenuSelect)) {
        $slogxtData['wizardFinalize'] = true;
    }
  }
  
}
