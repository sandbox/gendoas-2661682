<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbTabEditController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ....
 */
class TbTabEditController extends XtEditControllerBase {

  protected $vocabulary;
  protected $root_term;
  protected $opSave = false;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->isTargetEntitySelectList = FALSE;
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbtab_edit');
    $base_entity_id = $request->get('base_entity_id');
    // $source_tid > 0: rootterm id for which to taxonomy label (toolbar item)
    // $source_tid == 0 AND $base_entity_id is string: serverResolve, multiple stages
    $source_tid = (integer) $base_entity_id;
    if (empty($source_tid) && !empty($base_entity_id)) {
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $this->doTbMenuSelect = $result['doTbMenuSelect'];
      if ($result['type'] === 'client_resolve') {
        $this->client_resolve = $result['value'];
        return SlogXt::arrangeUrgentMessageForm();
      }
      elseif ($result['type'] === 'rootterm') {
            $this->doTbMenuSelect = FALSE;
            $root_term = $result['value'];
            $request->attributes->set('base_entity_id', $root_term);
            $source_tid = $root_term->id();
      }
      else {
        list($target, $targetentity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
        $this->doTbMenuSelect = TRUE;
        $this->isTargetEntitySelectList = TRUE;
        $request->attributes->set('path_target', $target);
        return SlogTb::getTargetSelectFormArg($target);
      }
    }

    if ($source_tid > 0) {
      $this->root_term = SlogTx::entityInstance('slogtx_rt', $source_tid);
      $request->attributes->set('base_entity_id', $this->root_term);
      $this->vocabulary = $this->root_term->getVocabulary();
      $this->lastCacheTags = SlogTb::getMenuBlockCacheTags($this->vocabulary->getToolbarId());

      $targetEntityPlugin = $this->vocabulary->getTargetEntityPlugin();
      if ($tbtab_edit_class = $targetEntityPlugin->getTbtabEditClass()) {
        // this is for overriding name and description of toolbar tabs
        $this->entity = $targetEntityPlugin->getCurrentEntity();
        if (!$this->entity) {
          $target_term = $this->root_term->getTargetTerm();
          $entity = $targetEntityPlugin->getEntityFromTargetTermName($target_term->label());
          if ($entity) {
            $this->entity = $targetEntityPlugin->setEntity($entity);
          }
        }

        if (!$this->entity) {
          $args = [
            '@voc' => $this->vocabulary->id(),
            '@type' => $targetEntityPlugin->getPluginId(),
            '@tid' => $this->root_term->id(),
          ];
          $message = t("Entity not found: @voc/@type/@tid", $args);
          throw new \LogicException($message);
        }

        $entity_type_id = $this->entity->getEntityTypeId();
        $request->request->set("vocabulary", $this->vocabulary);
        $request->request->set($entity_type_id, $this->entity);

        return $tbtab_edit_class;
      }
      else {
        // edit target term's name and description itself
        return $this->getEntityFormObject('slogtx_voc.default', $this->vocabulary);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('Select the target entity.');
    }
    else {
      return t('Edit %title', ['%title' => $this->vocabulary->headerPathLabel()]);
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return $this->t('Next');
    }
    else {
      return $this->t('Update');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->doTbMenuSelect) {
      $slogxtData = &$form_state->get('slogxtData');
      if ($this->isTargetEntitySelectList) {
        $slogxtData['serverResolved'] = TRUE;
      }
      return ['slogxtData' => $slogxtData];
    }
    else {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['runCommand'] = 'wizardFormEdit';
      return parent::buildContentResult($form, $form_state);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $allowed = [(string) t('Save'), (string) t('Save configuration')];
      $this->opSave = in_array($op, $allowed);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::postBuildForm();
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $form['status'] = $this->buildStatus($form['#form_id'], TRUE, $form_state);
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $allowed = ['name', 'description', 'status', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'] + ['captcha' => 'captcha'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif (in_array($field_name, ['name', 'description'])) {
        $field['#prefix'] = '<div class="slogxt-input-field js-form-wrapper form-wrapper">';
        $field['#suffix'] = '</div>';
      }
    }

    if (isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    } else {
        $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $vocabulary = SlogTx::getVocabulary($this->vocabulary->id(), TRUE); // reload
    $vid = $vocabulary->id();
    $this->setSavedMessage($vocabulary->getTbtabLabel());
    SlogTb::invalidateBlockCache($this->lastCacheTags);

    $pdRole = FALSE;
    if ($this->entity) {
      $entity = $vocabulary->getTargetEntityPlugin()->setEntity($this->entity);
      if (!empty($entity)) {
        $pdRole = $entity->id();
      }
    }

    list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vid);
    return [
      'command' => 'slogtb::finishedTbTabChange',
      'args' => [
        'vid' => $vid,
        'pathLabel' => $vocabulary->headerPathLabel(),
        'description' => $vocabulary->getTbtabDescription(),
        'rootterm_id' => $this->root_term->id(),
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
        'pdRole' => $pdRole,
      ],
    ];
  }

}
