<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbMenuEditController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\slogtb\Event\SlogtbEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;

/**
 * Defines a controller ....
 */
class TbMenuEditController extends TbMenuEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->isTbMenuSelectList = FALSE;
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbmenu_edit');
    $base_entity_id = $request->get('base_entity_id');
    // $source_tid < 0: rootterm id for which to select a menu term
    // $source_tid == 0 AND $base_entity_id is string: serverResolve, multiple stages
    // $source_tid > 0: menu term id (menu item to move)
    $source_tid = (integer) $base_entity_id;
    if (empty($source_tid) && !empty($base_entity_id)) {
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $this->doTbMenuSelect = $result['doTbMenuSelect'];
      if ($result['type'] === 'client_resolve') {
        $this->client_resolve = $result['value'];
        return SlogXt::arrangeUrgentMessageForm();
      }
      elseif ($result['type'] === 'rootterm') {
        $source_tid = -$result['value']->id();
        if (!empty($result['menuSelectAlter'])) {
          $request->attributes->set('menuSelectAlter', $result['menuSelectAlter']);
        }
      }
    }

    if (empty($source_tid) && !empty($base_entity_id)) {
      $this->doTbMenuSelect = TRUE;
      list($target, $targetentity_id, $entity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
      $request->attributes->set('path_target', $target);
      return SlogTb::getTargetSelectFormArg($target);
    }
    else {
      if ($source_tid < 0) {
        $request->attributes->set('pathReplaceKey', (string) $base_entity_id);
        $rootterm_id = -$source_tid;
        $rootterm = SlogTx::getRootTerm($rootterm_id);
        if ($rootterm && $rootterm->isRootTerm()) {
          $this->doTbMenuSelect = TRUE;
          $this->isTbMenuSelectList = TRUE;
          $request->attributes->set('root_term', $rootterm);
          $this->vocabulary = $rootterm->getVocabulary();
          return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
        }
        else {
          $message = t("Root term not found: @tid", ['@tid' => $rootterm_id]);
          throw new \LogicException($message);
        }
      }
      else {
        $this->menuTerm = SlogTx::getMenuTerm($source_tid);
        return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
      }
    }
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect) {
      $event = new SlogxtGenericEvent([&$form, $form_state, $form_id]);
      \Drupal::service('event_dispatcher')
          ->dispatch(SlogtbEvents::SLOGTB_MENU_FORM_ALTER, $event);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('Select menu item for edit.');
    }
    else {
      return t('Edit %title', ['%title' => $this->menuTerm->label()]);
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return $this->t('Next');
    }
    else {
      return $this->t('Update');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      $slogxtData = &$form_state->get('slogxtData');
      if ($this->isTbMenuSelectList) {
        $slogxtData['serverResolved'] = TRUE;
        if ($this->vocabulary->isSysToolbar()) {
          $slogxtData['infoMsg'] = t('Select the menu item to edit');
          $slogxtData['attachCommand'] = 'attachSysTbmenuList';
        }
      }
      else {
        $slogxtData['infoMsg'] = t('Select the type');
      }
      return ['slogxtData' => $slogxtData];
    }
    else {
      return parent::buildContentResult($form, $form_state);
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

//todo::next - captcha -['captcha' => 'captcha']
    $allowed = ['name', 'description', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'] + ['captcha' => 'captcha'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif ($field['#type'] === 'container' && isset($field['#access']) && $field['#access']) {
        $field['#attributes']['class'][] = 'slogxt-input-field';
      }
    }

    if (is_array($form['name']) && $form['name']['#description_display'] !== 'invisible') {
      $element = &$form['name']['widget'][0];
      $element['#description'] = t('The label of the menu item.');
      $element['value']['#description'] = $element['#description'];
    }
    if (is_array($form['description'])) {
      $element = &$form['description']['widget'][0];
      $element['#description'] = t('A description of the menu item.');
    }
    if (isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $menuTerm = SlogTx::getMenuTerm($this->menuTerm->id(), TRUE); // reload !!
    $this->setSavedMessage($menuTerm->label());

    if ($menuTerm) {
      $vocabulary = $menuTerm->getVocabulary();
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      return [
        'command' => 'slogtb::finishedTbMenuChange',
        'args' => [
          'tid' => $menuTerm->id(),
          'name' => $menuTerm->label(),
          'description' => $menuTerm->getDescription(),
          'toolbar' => $toolbar,
          'toolbartab' => $toolbartab,
        ],
      ];
    }

    return FALSE;
  }

}
