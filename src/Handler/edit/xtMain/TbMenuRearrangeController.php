<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbMenuRearrangeController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ....
 */
class TbMenuRearrangeController extends XtEditControllerBase {

  protected $rootTerm;
  protected $opSave = false;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->isTargetEntitySelectList = FALSE;
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbmenu_rearrange');
    $base_entity_id = $request->get('base_entity_id');
    // $source_tid > 0: rootterm id for which to rearrange menu items
    // $source_tid == 0 AND $base_entity_id is string: serverResolve, multiple stages
    $source_tid = (integer) $base_entity_id;
    if (empty($source_tid) && !empty($base_entity_id)) {
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $this->doTbMenuSelect = $result['doTbMenuSelect'];
      if ($result['type'] === 'client_resolve') {
        $this->client_resolve = $result['value'];
        return SlogXt::arrangeUrgentMessageForm();
      }
      elseif ($result['type'] === 'rootterm') {
        $this->doTbMenuSelect = FALSE;
        $request->attributes->set('base_entity_id', $result['value']);
        return ('\Drupal\slogtx_ui\Form\MenuTermOverview');
      }
      else {
        list($target, $targetentity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
        $this->doTbMenuSelect = TRUE;
        $this->isTargetEntitySelectList = TRUE;
        $request->attributes->set('path_target', $target);
        return SlogTb::getTargetSelectFormArg($target);
      }
    }
    elseif ($source_tid > 0) {
      $root_term = SlogTx::entityInstance('slogtx_rt', $source_tid);
      $request->attributes->set('base_entity_id', $root_term);
      return ('\Drupal\slogtx_ui\Form\MenuTermOverview');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('Select the target entity.');
    }
    else {
      $vocabulary = $this->rootTerm->getVocabulary();
      return t('Rearrange %title', ['%title' => $vocabulary->headerPathLabel()]);
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return $this->t('Next');
    }
    else {
      return $this->t('Save');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->doTbMenuSelect) {
      $slogxtData = &$form_state->get('slogxtData');
      if ($this->isTargetEntitySelectList) {
        $slogxtData['serverResolved'] = TRUE;
      }
      return ['slogxtData' => $slogxtData];
    }
    else {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $slogxtData['tabledrag'] = 'taxonomy';
      
      $this->isEmptyList = FALSE;
      $result = parent::buildContentResult($form, $form_state);
      if ($this->isEmptyList) {
        $slogxtData = &$result['slogxtData'];
        $slogxtData['htmlContent'] = FALSE;
        $slogxtData['tabledrag'] = FALSE;
        $slogxtData['emptyWarning'] = t('There is no content in this list.');
      }
      
      return $result;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $request = \Drupal::request();
    $this->rootTerm = $request->get('base_entity_id');
    if (!$this->rootTerm) {
      $message = t('Root term not found.');
      throw new \LogicException($message);
    }

    // 
    $op = $request->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      return;
    }


    $form['#attached']['library'][] = 'slogtx_ui/tabledrag';
    $form['terms']['#header'] = [];
    if (is_array($form['terms'])) {
      $children = Element::children($form['terms']);
      $this->isEmptyList = (count($children) === 0);
      foreach ($children as $term_key) {
        $termdata = & $form['terms'][$term_key];
        unset($termdata['operations']);
        $menu_term = $termdata['#term'];
        $termdata['term'] = array_merge_recursive($termdata['term'], [
          '#type' => 'markup',
          '#markup' => $menu_term->label(),
        ]);
        unset($termdata['term']['#url']);
      }
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    } else {
        $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();

    $rootTerm = $this->rootTerm;
    if ($rootTerm) {
      $vocabulary = $rootTerm->getVocabulary();
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      return [
        'command' => 'slogtb::finishedTbMenuRearrange',
        'args' => [
          'tid' => $rootTerm->id(),
          'name' => $rootTerm->label(),
          'description' => $rootTerm->getDescription(),
          'toolbar' => $toolbar,
          'toolbartab' => $toolbartab,
        ],
      ];
    }

    return FALSE;
  }

}
