<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbMenuMoveController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Entity\RootTerm;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ....
 */
class TbMenuMoveController extends TbMenuEditControllerBase {

  protected $targetRootTerm = NULL;
  
  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {

    /**
     *              FROM        \    TO |  A  |  B  |  C  |  D  |
     *  ---------------------------------------------------------
     *              A. TbMenuMain       | Sc1 | Sc2 | --- | --- |
     *  ---------------------------------------------------------
     *              B. SysArchMain      | Sc3 | Sc4 | --- | --- |
     *  ---------------------------------------------------------
     *              C. TbMenuNode       | --- | --- | Sc5 | Sc6 |
     *  ---------------------------------------------------------
     *              D. SysArchNode      | --- | --- | Sc7 | Sc8 |
     *  ---------------------------------------------------------
     *  
     *  A. TbMenuMain : home, role, user, earth, help, ...
     *  B. SysArchMain: _sys__archive_home, _sys__archive_role, _sys__trash_home, ...
     *  C. TbMenuNode : depends on node
     *  D. SysArchNode: _sys__archive_sys_node/Node.? (?: node.id)
     * 
     *  Scenarios:
     *  Sc1:        one step:: tbmenu/141/move/30 (all resolved clientside)
     *  Sc2:        one step:: tbmenu/147/move/248 (all resolved clientside)
     *  Sc3:        archive_main->main - 3 steps::
     *                  step 1: tbmenu/archive/move/{next_entity_id} (archive to resolve)
     *                  step 2: tbmenu/archive~home/move/{next_entity_id} (archive~home to resolve)
     *                  step 3: tbmenu/298/move/30 (all resolved)
     *  Sc4:        archive_main->archive_main - 3 steps::
     *                  step 1: tbmenu/archive/move/{next_entity_id} (archive to resolve)
     *                  step 2: tbmenu/archive~home/move/{next_entity_id} (archive~home to resolve)
     *                  step 3: tbmenu/298/move/258 (e.g. archive->trash, all resolved)
     *  Sc5:        one step:: tbmenu/282/move/299 (all resolved clientside)
     *  Sc6:        one step:: tbmenu/282/move/archive~_node~_sys (target resolved by source)
     *  Sc7:        archive_node->node - 4 steps::
     *                  step 1: tbmenu/archive/move/{next_entity_id} (archive to resolve)
     *                  step 2: tbmenu/archive~_node/move/{next_entity_id} (archive~_node to resolve)
     *                  step 3: tbmenu/archive~_node~344/move/{next_entity_id} (archive~_node~344 to resolve)
     *                  step 4: tbmenu/284/move/299 (e.g. archive_node->node, all resolved)
     *  Sc8:        archive_node->archive_node - 4 steps::
     *                  step 1: tbmenu/archive/move/{next_entity_id} (archive to resolve)
     *                  step 2: tbmenu/archive~_node/move/{next_entity_id} (archive~_node to resolve)
     *                  step 3: tbmenu/archive~_node~344/move/{next_entity_id} (archive~_node~344 to resolve)
     *                  step 4: tbmenu/284/move/333 (e.g. archive_node->trash_node, all resolved)
     * 
     * 
     *  JavaScript Scenarios (finishedTbMenuMove):
     *  Sc1:      Ac0, Ac1
     *    Sc1.new:        xx 
     *  Sc2:      Ac2
     *  Sc3:      xx
     *    Sc3.new:        xx
     *  Sc4:      none
     * 
     *  Sc5:      Ac8
     *  Sc6:      Ac8
     *  Sc7:      Ac8
     *  Sc8:      none
     * 
     *  Sc9 (prepare new toolbar item): Ac9
     * 
     * 
     * JavaScript Actions:
     * Ac0:   targetList: activate
     * Ac1:   targetList: refresh
     * Ac2:   targetList: add to history
     * Ac3:   sourceList: remove from history
     * Ac4:   xx
     * Ac5:   xx
     * Ac6:   xx
     * Ac7:   xx
     * 
     * Ac8:   content: refresh if active (source or target)
     * Ac9:   reload  -   see $this->finishedData['reload']
     * 
     * 
     * ???? fallback tid for each toolbar, not movable ????
     */
    $this->doTbMenuSelect = FALSE;
    $this->isTbMenuSelectList = FALSE;
    $this->targetEntityId = FALSE;
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbmenu_move');
    $base_entity_id = $request->get('base_entity_id');

    // $source_tid > 0: menu term id (menu item to move)
    // $base_entity_id not empty: string to resolve, e.g. 'archive~home'
    $source_tid = (integer) $base_entity_id;
    if (empty($source_tid) && !empty($base_entity_id)) {
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $this->doTbMenuSelect = $result['doTbMenuSelect'];
      if ($result['type'] === 'client_resolve') {
        /**
         * tbmenu/archive~_node/move/{next_entity_id} - Sc7.2, Sc8.2 (archive~_node to resolve, 2. step)
         * node.id is required - resolve clientside and come again
         */
        $this->client_resolve = $result['value'];
        return SlogXt::arrangeUrgentMessageForm();
      }
      elseif ($result['type'] === 'rootterm') {
        /**
         * tbmenu/archive~home/move/{next_entity_id} - Sc3.2, Sc4.2 (archive~home to resolve, 2. step)
         * tbmenu/archive~_node~344/move/{next_entity_id} - Sc7.3, Sc8.3 (archive~_node~344 to resolve, 3. step)
         */
        $source_tid = -$result['value']->id();
      }
    }

    if (empty($source_tid) && !empty($base_entity_id)) {
      /**
       * tbmenu/archive/move/{next_entity_id} - Sc3.1, Sc4.1, Sc7.1, Sc8.1 (archive to resolve, 1. step)
       */
      $this->doTbMenuSelect = TRUE;
      list($target, $targetentity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
      $request->attributes->set('path_target', $target);
      return SlogTb::getTargetSelectFormArg($target);
    }
    else {
      if ($source_tid < 0) {
        /**
         * ... tbmenu/archive~home/move/{next_entity_id} - Sc3.2, Sc4.2 (archive~home to resolve, ... 2. step)
         * ... tbmenu/archive~_node~344/move/{next_entity_id} - Sc7.3, Sc8.3 (archive~_node~344 to resolve, ... 3. step)
         */
        $request->attributes->set('pathReplaceKey', $base_entity_id);
        $rootterm_id = - $source_tid;
        $rootterm = SlogTx::getRootTerm($rootterm_id);
        if ($rootterm && $rootterm->isRootTerm()) {
          $this->doTbMenuSelect = TRUE;
          $this->isTbMenuSelectList = TRUE;
          $request->attributes->set('root_term', $rootterm);
          $this->vocabulary = $rootterm->getVocabulary();
          list($target, $targetentity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
          $this->targetEntityId = $targetentity_id;
          return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
        }
        else {
          $message = t("Root term not found: @tid", ['@tid' => $rootterm_id]);
          throw new \LogicException($message);
        }
      }
      else {
        $this->menuTerm = $menu_term = SlogTx::getMenuTerm($source_tid);
        $next_entity_id = $request->get('next_entity_id');
        $target_rootterm_id = (integer) $next_entity_id;
        list($target, $targetentity_id, $xtra) = explode(SlogXt::XTURL_DELIMITER, $next_entity_id . SlogXt::XTURL_DELIMITER3);

        if ($target === 'prepare' && !empty($targetentity_id)) {
          $toolbar_id = $targetentity_id;
          if (!SlogTb::isToolbarPrepareable($toolbar_id)) {
            $message = t('Toolbar is not preparable: @toolbar', ['@toolbar' => $toolbar_id]);
            throw new \LogicException($message);
          }
          else {
            $this->targetRootTerm = SlogTx::getTbObjectToPrepare($toolbar_id);
          }
        }
        elseif ($target_rootterm_id > 0) {
          $ste_plugin = $this->menuTerm->getVocabulary()->getTargetEntityPlugin();
          if ($ste_plugin->isEntitySettable()) {
            /**
             * tbmenu/284/move/270 - 183:menu_tid, 270:rootterm_tid for 'Default' target term
             * we have to find the root term for the entity dependent target term (e.g. Node.344)
             * four steps: 
             *     Sc7.4 (archive_node->node, 4. step)
             *     Sc8.4 (archive_node->archive_node, e.g. archive->trash, 4. step)
             */
            $targetterm = $this->menuTerm->getTargetTerm();
            $t_rootterm = SlogTx::getRootTerm($target_rootterm_id);
            $t_rootterm->getVocabulary()->setCurrentTargetTermName($targetterm->label());
            $this->targetRootTerm = $t_rootterm->getVocabulary()->getCurrentRootTerm();
          }
          else {
            /**
             * tbmenu/183/move/30 - 183:menu_tid, 30:target rootterm_tid
             * one step: Sc1 (main->main), Sc2 (main->archive_main), Sc5 (node->node)
             * three steps: 
             *     Sc3.3 (archive_main->main), Sc4.3 (archive_main->archive_main)
             */
            $this->targetRootTerm = SlogTx::getRootTerm($target_rootterm_id);
          }
        }
        elseif ($target_rootterm_id === 0 && !empty($next_entity_id)) {
          if (!empty($xtra) && $xtra === SlogTx::TOOLBAR_ID_SYS) {
            /**
             * tbmenu/282/move/archive~_node~_sys
             * one step: Sc6 (node->archive_node, target resolved by source)
             */
            $entity = NULL;
            $toolbar_id = $targetentity_id;
            $toolbar = SlogTx::getToolbar($toolbar_id);
            $enforce_teid = $toolbar->getEnforceTargetEntity();
            $te_plugins = SlogTx::getTargetEntityPlugins();
            $te_plugin = $te_plugins[$enforce_teid];
            if (empty($te_plugin)) {
              $args = [
                  '@target' => 'next_entity_id',
                  '@next_entity_id' => $next_entity_id,
              ];
              $message = t('Plugin not found: @target / @next_entity_id', $args);
              throw new \LogicException($message);
            }

            if ($te_plugin->isEntitySettable()) {
              $vocabulary = $menu_term->getVocabulary();
              $term_name = $menu_term->getTargetTerm()->label();

              if ($toolbar_id === SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
                $base_node_id = $te_plugin->getTargetEntityId($term_name);
                $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($target);   //$target: 'archive', 'trash'
                $entity = Node::load($base_node_id);
                if (!$rootterm = SlogTx::getSysSysRootTerm($plugin_id, $entity)) {
                  $args = ['@label' => $entity->label()];
                  return SlogXt::arrangeUrgentMessageForm(t('Not prepared yet: @label', $args), 'warning');
                }
              }
              else {
                if (!$entity = $vocabulary->getEntityFromTargetTermName($term_name)) {
                  $args = [
                      '@entitytype' => $targetentity_id,
                      '@term_name' => $term_name,
                  ];
                  $message = t('Entity not found: @entitytype / @term_name', $args);
                  throw new \LogicException($message);
                }
              }
            }

            $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($target);
            $this->targetRootTerm = SlogTx::getSysSysRootTerm($plugin_id, $entity);
          }
        }

        if (!$this->targetRootTerm || !$this->targetRootTerm instanceof RootTerm) {
          $args = ['@tid' => $target_rootterm_id];
          $message = t('Root term not found: @tid', $args);
          throw new \LogicException($message);
        }

        $class = 'Drupal\slogtb\Form\TbMenuMoveForm';
        $form_arg = $this->classResolver->getInstanceFromDefinition($class);
        $form_arg->setEntity($menu_term);
        $form_arg->setModuleHandler(\Drupal::moduleHandler());
        return $form_arg;
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('What to move');
    }
    elseif ($this->targetRootTerm) {
      $to_vocab = $this->targetRootTerm->getVocabulary();
      $path_label = $to_vocab->isNoneTargetEntity() ? $to_vocab->pathLabel() : $to_vocab->headerPathLabel();
      $args = [
          '%name' => $this->menuTerm->label(),
          '%to' => $path_label,
      ];
      return t('Move %name to %to', $args);
    }
    else {
      return t('Move not available');
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return $this->t('Next');
    }
    else {
      return $this->t('Move');
    }
  }

  protected function jsAlterFormArgs() {
    $items = $this->getTbMenuContentItems();
    return [
        'items' => $items,
        'hasItems' => (count($items) > 0),
        'radios' => FALSE,
        'enableSubmit' => TRUE,
    ];
  }

  protected function getTbMenuContentItems() {
    $tid = $this->menuTerm->id();
    return $this->getSlogtbHandler()->getTbMenuContentItems($tid);
  }

  protected function getTbMenuHasChildren() {
    if (!isset($this->menu_has_children)) {
      $this->menu_has_children = $this->menuTerm->hasChildren();
    }
    return $this->menu_has_children;
  }

  public function getTbMenuSubtree() {
    return $this->menuTerm->getSubtree();
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect && $this->targetRootTerm) {
      $name = $this->menuTerm->label();
      $vocabulary = $this->menuTerm->getVocabulary();
      if ($entity = $this->menuTerm->getSettableEntity()) {
        $vocabulary->setCurrentTargetEntity($entity);
        $this->targetRootTerm->getVocabulary()->setCurrentTargetEntity($entity);
      }

      $voc_path = $vocabulary->headerPathLabel();
      $args = ['%name' => $name, '%path' => $voc_path];
      $msg = '- ' . t('You are about to move the menu item <br/>%name (%path)', $args);
      $msg .= '<br />- ';
      $items = $this->getTbMenuContentItems();
      if (!empty($items)) {
        $msg .= t('Be aware of the listed contents.');
      }
      else {
        $msg .= t('This menu item has no own contents.');
      }
      $msg .= '<br />';
      if ($this->getTbMenuHasChildren()) {
        $msg .= '*** ' . t('WARNING: This menu item contains sub menues:<br />');
        $tree = $this->getTbMenuSubtree();
        $t_items = [];
        foreach ($tree as $t_item) {
          $t_items[] = $t_item->name;
        }
        $msg .= '(' . implode(', ', $t_items) . ')';
      }
      else {
        $msg .= '- ' . t('This menu item has no sub menues.');
      }
      $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');

      // replace submit handler
      $controllerClass = get_class($this);
      $actions = &$form['actions'];
      $actions['submit']['#submit'] = ["$controllerClass::formSubmit"];
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      if ($this->isTbMenuSelectList) {
        $slogxtData['serverResolved'] = TRUE; // finished server resolve
        $slogxtData['targetEntityId'] = $this->targetEntityId;
        if ($this->vocabulary->isSysToolbar()) {
          $slogxtData['infoMsg'] = t('Select the menu item to move');
          $slogxtData['attachCommand'] = 'attachSysTbmenuList';
        }
      }
      else {
        $slogxtData['infoMsg'] = t('Select the type');
      }
      return ['slogxtData' => $slogxtData];
    }
    else {
      $provider = SlogTb::getSlogtbHandler()->getProvider();
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $slogxtData['alterForm'] = [
          'command' => "$provider::alterFormAddList",
          'args' => $this->jsAlterFormArgs(),
      ];
      return parent::buildContentResult($form, $form_state);
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $allowed = ['notice', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
    }

    if (is_array($form['description'])) {
      $element = &$form['description']['widget'][0];
      $element['#description'] = t('A description of the menu item.');
    }
    if (isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = TRUE;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $this->setSubmitDanger($form);
      $slogxtData['autoEnableSubmit'] = TRUE;
      $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * ...
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $menu_term = $calledObject->menuTerm;
    $target_root_term = $calledObject->targetRootTerm;

    if ($menu_term && $target_root_term) {
      // prepare finished data
      $move_tid = $menu_term->id();
      $source_vocabulary = $menu_term->getVocabulary();
      list($s_toolbar, $s_toolbartab) = SlogTx::getVocabularyIdParts($source_vocabulary->id());
      $toolbar = $source_vocabulary->getToolbar();
      $is_node_subsys = $toolbar->isNodeSubsysToolbar();
      $is_sys_toolbar = $toolbar->isSysToolbar();
      $moved_tids = [$move_tid];
      if ($calledObject->getTbMenuHasChildren()) {
        $tree = $calledObject->getTbMenuSubtree();
        foreach ($tree as $item) {
          $moved_tids[] = $item->tid;
        }
      }

      $target_vocab = $calledObject->targetRootTerm->getVocabulary();
      list($t_toolbar, $t_toolbartab) = SlogTx::getVocabularyIdParts($target_vocab->id());
      $target_tb = $target_vocab->getToolbar();
      $target_is_node_subsys = $target_tb->isNodeSubsysToolbar();
      $target_is_sys_toolbar = $target_tb->isSysToolbar();

      if ($entity = $menu_term->getSettableEntity()) {
        $source_vocabulary->setCurrentTargetEntity($entity);
        $target_vocab->setCurrentTargetEntity($entity);
      }

      $with_node_subsys = $is_node_subsys || $target_is_node_subsys;
      $both_sys_toolbar = $is_sys_toolbar && $target_is_sys_toolbar;
      $calledObject->finishedData = [
          'moved_tids' => $moved_tids,
          'moved_tid' => $move_tid,
          'moved_path' => SlogTb::getSlogtbHandler()->getPathPrefix() . $move_tid,
          'withNodeSubsys' => $with_node_subsys,
          'sPathLabel' => $source_vocabulary->headerPathLabel(),
          'sToolbar' => $s_toolbar,
          'sToolbartab' => $s_toolbartab,
          'tPathLabel' => $target_vocab->headerPathLabel(),
          'tToolbar' => $t_toolbar,
          'tToolbartab' => $t_toolbartab,
          'reload' => FALSE,
      ];

      // find ids before moving
      $testToolbar = $is_sys_toolbar ? $target_tb : $toolbar;
      $rt_ids_old = $testToolbar->getCurrentRootTermIDs(TRUE);
      // move menu items now
      $menu_term->moveToRootTerm($target_root_term);
      // ids after moving
      $rt_ids_new = $testToolbar->getCurrentRootTermIDs(TRUE, TRUE);

      //
      if ($with_node_subsys && $entity) {
        $calledObject->finishedData['node_id'] = $entity->id();
      }
      else {
        $calledObject->finishedData['reload'] = $both_sys_toolbar ? FALSE : ($rt_ids_new !== $rt_ids_old);
      }
    }
    else {
      $mobjects = [];
      if (!isset($menu_term)) {
        $mobjects[] = 'menu term';
      }
      if (!isset($target_root_term)) {
        $mobjects[] = 'target root term';
      }
      $args = ['@obj' => implode('/', $mobjects)];
      $msg = t('Missing object for moving menu term (@obj)', $args);
      $calledObject->messenger->addError($msg);
      SlogTx::logger()->error($msg);
      $calledObject->setSubmitError(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    if (!$this->hasSubmitError()) {
      // clear messages list, set finished message
      $this->messenger->deleteAll();
      $msg_args = [
          '%target' => $this->targetRootTerm->getVocabulary()->headerPathLabel(),
          '%item' => $this->menuTerm->label(),
      ];
      $this->messenger->addStatus(t('Menu item %item was moved to %target.', $msg_args));
      if (!empty($this->finishedData['reload'])) {
        $this->messenger->addStatus(t('Reloading is required.'));
      }
      return [
          'command' => 'slogtb::finishedTbMenuMove',
          'args' => $this->finishedData,
      ];
    }
  }

}
