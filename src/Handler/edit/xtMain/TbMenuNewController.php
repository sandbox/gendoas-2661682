<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\slogtb\Event\SlogtbEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;

/**
 * Defines a controller ....
 */
class TbMenuNewController extends TbMenuEditControllerBase {

  protected $root_term = FALSE;
  protected $do_rootterm_create = FALSE;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbmenu_new');
    $base_entity_id = $request->get('base_entity_id');

    // $source_tid == 0 AND $base_entity_id is string: serverResolve, multiple stages
    // $source_tid > 0: rootterm id (where to add new menu item)
    $source_tid = (integer) $base_entity_id;
    if (empty($source_tid) && !empty($base_entity_id)) {
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $this->doTbMenuSelect = $result['doTbMenuSelect'];
      if ($result['type'] === 'client_resolve') {
        $this->client_resolve = $result['value'];
        return SlogXt::arrangeUrgentMessageForm();
      }
      elseif ($result['type'] === 'rootterm') {
        $this->doTbMenuSelect = FALSE;
        $source_tid = $result['value']->id();
      }
      elseif ($result['type'] === 'create_new') {
        $this->doTbMenuSelect = FALSE;
        $this->do_rootterm_create = TRUE;
        $this->rtget_plugin = SlogTx::getSysSysRtgetPlugin($result['rtget_plugin_id'], NULL);
        $this->menuTerm = MenuTerm::create([
                    'vid' => $this->rtget_plugin->getVocabulary()->id(),
                    'parent' => 0,
        ]);
        return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
      }
    }

    if (empty($source_tid) && !empty($base_entity_id)) {
      $this->doTbMenuSelect = TRUE;
      list($target, $targetentity_id, $entity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
      $request->attributes->set('path_target', $target);
      return SlogTb::getTargetSelectFormArg($target);
    }
    else {
      $tid = $source_tid;
      $this->root_term = $rootterm = SlogTx::getRootTerm($tid);
      if ($rootterm && $rootterm->isRootTerm()) {
        $request->attributes->set('slogtx_rt', $rootterm);
        $vocabulary = $rootterm->getVocabulary();
        $this->menuTerm = MenuTerm::create([
                    'vid' => $vocabulary->id(),
                    'parent' => $rootterm->id(),
        ]);
      }
      if (!$rootterm || !$rootterm->isRootTerm()) {
        $message = t("Root term not found: @tid", ['@tid' => $tid]);
        throw new \LogicException($message);
      }

      return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
    }
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect) {
      $event = new SlogxtGenericEvent([&$form, $form_state, $form_id]);
      \Drupal::service('event_dispatcher')
              ->dispatch(SlogtbEvents::SLOGTB_MENU_FORM_ALTER, $event);
      if ($this->do_rootterm_create && $actions = &$form['actions']) {
        $controllerClass = get_class($this);
        $form['#validate'][] = "$controllerClass::formValidate";
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('Select menu item.');
    }
    else {
      return t('Create menu item');
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return $this->t('Next');
    }
    else {
      return $this->t('Create');
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      $slogxtData = & $form_state->get('slogxtData');
      $slogxtData['infoMsg'] = t('Select the type for new menu item');
      return;
    }

    $allowed = ['name', 'description', 'label', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token', 'link_sids'] + ['captcha' => 'captcha'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        if ($field_name === 'relations') {
          // weight, on top, is positioned later
          $field['weight']['#value'] = -1000;
          // hide relations
          $field['#attributes']['class'][] = 'visually-hidden';
        }
        elseif ($field_name !== 'relations') {
          $form[$field_name]['#access'] = FALSE;
        }
      }
      elseif ($field['#type'] === 'container' && isset($field['#access']) && $field['#access']) {
        $field['#attributes']['class'][] = 'slogxt-input-field';
      }
    }

    parent::preRenderFormLast($form, $form_state);
  }

  /**
   * 
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;   // do not create root term
    }

    $called_object = self::calledObject();
    if ($called_object->do_rootterm_create) {
      $rtget_plugin = $called_object->rtget_plugin;
      $target_tbid = $rtget_plugin->get('toolbar_id') ?? '';
      if ($target_tbid !== 'user') {
        $msg = t('Unexpected error: not supported.');
        $form_state->setErrorByName('', t('Unexpected error: not supported.'));
      }

      $user = SlogXt::getUserEntity();
      $rtget_plugin->getVocabulary()->setCurrentTargetEntity($user);
      $rtget_plugin->setTargetTermByName(NULL, TRUE); // create target term
      $root_term = $rtget_plugin->getRootTermByName();
      if (!$root_term) {
        $message = t("Root term not created for @vid", ['@vid' => $rtget_plugin->getVocabulary()->id()]);
        throw new \LogicException($message);
      }

      $called_object->root_term = $root_term;
      $called_object->menuTerm->setParentID($root_term->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();

    if ($this->do_rootterm_create && $this->root_term) {
      return ['command' => 'slogxt::reload'];
    }

    //
    $rootTerm = $this->root_term;
    if ($rootTerm) {
      $vocabulary = $rootTerm->getVocabulary();
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      return [
          'command' => 'slogtb::finishedTbMenuAdd',
          'args' => [
              'tid' => $rootTerm->id(),
              'name' => $rootTerm->label(),
              'description' => $rootTerm->getDescription(),
              'toolbar' => $toolbar,
              'toolbartab' => $toolbartab,
              'isSysToolbar' => $vocabulary->getToolbar()->isSysToolbar(),
          ],
      ];
    }

    return FALSE;
  }

}
