<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\edit\xtMain\TbTabNewController.
 */

namespace Drupal\slogtb\Handler\edit\xtMain;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\slogtx\Interfaces\TxVocabularyInterface;
use Drupal\slogtx\Entity\RootTerm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController;

/**
 * Defines a controller ....
 */
class TbTabNewController extends TbMenuNewController {

  protected $doToolbarSelect = FALSE;
  protected $root_term = FALSE;
  protected $vocabulary = FALSE;
  protected $do_rootterm_create = FALSE;
  protected $hasChildren = FALSE;
  protected $lastCacheTags = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $request->attributes->set('action_plugin_id', 'slogtb_edit_tbtab_new');
    $toolbar_id = $request->get('base_entity_id');

    if (!empty($toolbar_id) && $toolbar_id !== '{base_entity_id}' && ($toolbar = SlogTx::getToolbar($toolbar_id))) {
      $this->doToolbarSelect = FALSE;
      $this->toolbar = $toolbar;
      $this->toolbar_id = $toolbar_id;
      $request->attributes->set('base_entity_id', NULL);
      $tb_object = SlogTx::getTbObjectToPrepare($toolbar_id);
      if ($tb_object && $tb_object instanceof RootTerm) {
        $root_term = $tb_object;
        $this->lastCacheTags = SlogTb::getMenuBlockCacheTags($toolbar_id);
        $this->root_term = $root_term;
        $this->hasChildren = $root_term->hasChildren();
        if ($this->hasChildren) {
          $target_term = $root_term->getTargetTerm();
          $request->attributes->set($target_term->getEntityTypeId(), $target_term);
          $class = 'Drupal\slogtx_ui\Form\TargetTermEnableForm';
          $form_arg = $this->classResolver->getInstanceFromDefinition($class);
          $form_arg->setEntity($target_term);
          $form_arg->setModuleHandler(\Drupal::moduleHandler());
          return $form_arg;
        }
        else {
          $this->menuTerm = MenuTerm::create([
              'vid' => $root_term->getVocabularyId(),
              'parent' => $root_term->id(),
          ]);
          return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
        }
      }
      elseif ($tb_object && $tb_object instanceof TxVocabularyInterface) {
        $this->root_term = FALSE;
        $this->vocabulary = $tb_object;
        $this->do_rootterm_create = TRUE;
        $this->menuTerm = MenuTerm::create([
            'vid' => $this->vocabulary->id(),
            'parent' => 0,
        ]);

        return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
      }
    }
    else {
      $this->doToolbarSelect = TRUE;
      $request->attributes->set('path_target', '{base_entity_id}');
      return 'Drupal\slogtb\Form\TbPreparableSelectForm';
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doToolbarSelect) {
      // add submit handler
      $actions = &$form['actions'];
      if (isset($actions['submit'])) {
        $controllerClass = get_class($this);
        $form['#validate'][] = "$controllerClass::formValidate";
        $actions['submit']['#submit'][] = "$controllerClass::formSubmit";
      }
      if (isset($actions['cancel'])) {
        unset($actions['cancel']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getVocabLabel() {
    if ($this->do_rootterm_create && $this->vocabulary) {
      return $this->vocabulary->getTbtabLabel();
    }
    elseif (!$this->do_rootterm_create && $this->root_term) {
      return $this->root_term->getVocabulary()->getTbtabLabel();
    }
    else {
      return '???';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormTitle() {
    if ($this->doToolbarSelect) {
      return t('Select toolbar for a new item');
    }
    else {
      $args = [
          '%toolbar' => $this->toolbar_id,
          '%name' => $this->getVocabLabel(),
      ];
      if ($this->hasChildren) {
        return t('Activate %toolbar item: %name', $args);
      }
      else {
        return t('Create menu item for new %toolbar tab: %name', $args);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getSubmitLabel() {
    if ($this->doToolbarSelect) {
      return t('Next');
    }
    elseif ($this->hasChildren) {
      return t('Activate');
    }
    else {
      return t('Create');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $this->doTbMenuSelect = $this->doToolbarSelect;
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    if ($this->doToolbarSelect) {
      $slogxtData = & $form_state->get('slogxtData');
      $slogxtData['infoMsg'] = t('Select the toolbar for which to add a new item');
      return;
    }

    if (!$this->doToolbarSelect) {
      if ($this->hasChildren) {
        $this->preRenderFormBase($form, $form_state);

        $msg = t('You are about to activate %name', ['%name' => $this->getVocabLabel()]);
        $form['description']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
        parent::preRenderFormLast($form, $form_state);
      }
      else {
        parent::preRenderForm($form, $form_state);
      }
    }
  }

  public static function formValidate(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;   // do not create root term
    }

    $called_object = self::calledObject();
    $root_term = $called_object->root_term;
    if (!$root_term && $called_object->do_rootterm_create) {
      $tb_object = SlogTx::getTbObjectToPrepare($called_object->toolbar_id, NULL, TRUE);
      if (!$tb_object || !$tb_object instanceof RootTerm) {
        $message = t("Root term not created for @vid", ['@vid' => $called_object->vocabulary->id()]);
        throw new \LogicException($message);
      }

      $called_object->root_term = $tb_object;
      $called_object->menuTerm->setParentID($called_object->root_term->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $called_object = self::calledObject();
    $root_term = $called_object->root_term;
    $root_term->getTargetTerm()->setStatus(TRUE)->save();
    $root_term->setStatus(TRUE)->save();
    if ($called_object->lastCacheTags) {
      SlogTb::invalidateBlockCache($called_object->lastCacheTags);
    }
    $called_object->opSave = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();
    return ['command' => 'slogxt::reload'];
  }

}
