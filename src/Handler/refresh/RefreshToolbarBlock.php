<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\refresh\RefreshToolbarBlock.
 */

namespace Drupal\slogtb\Handler\refresh;

use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\slogxt\XtUserData;
use Drupal\Component\Plugin\PluginBase;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

/**
 * Defines a controller for ...
 */
class RefreshToolbarBlock extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $slogtx_tb = \Drupal::request()->get('slogtx_tb');
    $toolbar = SlogTx::entityInstance('slogtx_tb', $slogtx_tb);
    $default_role = XtUserData::getDefaultRole(\Drupal::currentUser());
    if (!$toolbar->isSysToolbar() && $toolbar->hasRoleTargetEntity()) {
      $toolbar_id = $toolbar->id();
      $plugin_id = SlogTb::STB_ID_TOOLBAR . PluginBase::DERIVATIVE_SEPARATOR . $toolbar_id;
      $block = SlogTb::getBlockByPluginId($plugin_id);
      $build = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block, 'block');
      $html = (string) \Drupal::service('renderer')->renderRoot($build);
      $toobarData = SlogTx::getJsToolbarData($toolbar_id);

      $data = [
          'defaultRole' => $default_role,
          'content' => $html,
          'settings' => ['toolbarData' => $toobarData],
      ];
    }

    if (empty($data)) {
      $data = [
          'error' => TRUE,
          'message' => 'RefreshToolbarBlock::responseData(): Unknown error.'
      ];
    }

    return $data;
  }

}
