<?php

/**
 * @file
 * Contains \Drupal\slogtb\Handler\refresh\RefreshMenuTop.
 */

namespace Drupal\slogtb\Handler\refresh;

use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\block\BlockViewBuilder;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

/**
 * Defines a controller ...
 */
class RefreshMenuTop extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $slogtx_voc = $request->get('slogtx_voc');
    $slogtx_rt = $request->get('slogtx_rt');
    $err_msg = FALSE;

    $vocabulary = SlogTx::entityInstance('slogtx_voc', $slogtx_voc);
    if (empty($slogtx_rt) || $vocabulary->setCurrentByRootTerm($slogtx_rt)) {
      $toolbar = $vocabulary->getToolbar();
      if (!$toolbar->isSysToolbar()) {
        // build for the asked vocabulary only (see buildToolbarTabItems::SlogtbElement)
        $toolbar_id = $toolbar->id();
        if ($block = SlogTb::getSlogtbMenuBlock($toolbar_id)) {
          $block_build = BlockViewBuilder::lazyBuilder($block->id(), 'full');
          $html = \Drupal::service('renderer')->renderPlain($block_build);

          $vid = $vocabulary->id();
          $toobarData = SlogTx::getJsToolbarData($toolbar_id);
          $toolbartab = SlogTx::getToolbartabFromVid($vid);
          $data = [
              'html' => (string) $html,
              'selector' => "#slogtb-item-$vid-tray .toolbar-menu-$vid",
              'tbdMenutabs' => $toobarData['menutabs'][$toolbartab],
          ];
        }
        else {
          $err_msg = 'Block not found: ' . $block_id;
        }
      }
      else {
        $err_msg = 'not allowed for _sys.';
      }
    }

    if (empty($data)) {
      if (empty($err_msg)) {
        $err_msg = 'unknown error.';
      }
      $err_msg = "RefreshMenuTop::responseData(): $err_msg";
      throw new \LogicException($err_msg);
    }

    return $data;
  }

}
