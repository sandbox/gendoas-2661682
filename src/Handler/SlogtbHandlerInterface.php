<?php

/**
 * @file
 * Definition of \Drupal\slogtb\Handler\SlogtbHandlerInterface.
 */

namespace Drupal\slogtb\Handler;

interface SlogtbHandlerInterface {
  
  /**
   * Return the own module name.
   * 
   * @return string
   */
  public function getProvider();
  
  /**
   * Return required libraries to be loaded.
   * 
   * @return array
   */
  public function getLibraries();
  
  /**
   * Return the libraries to be autoloaded on clientside.
   * 
   * @return array
   */
  public function getLibrariesAutoload();
  
  /**
   * Return the route name.
   * 
   * First used for SlogtbOnMenuItemSelect (default).
   * 
   * @return string
   */
  public function getRouteName($route_name_ext = '.on.menu_item_select');
  
  /**
   * Return the path prefix for SlogtbOnMenuItemSelect.
   * 
   * @return string
   *  The path prefix is without leading slash and with trailing slash.
   */
  public function getPathPrefix();
  
  /**
   * Return the JS selector where to insert the content.
   * 
   * @return string
   */
  public function getContentTarget();
  
  /**
   * Return the subitems (content items) of a menu item, if any.
   * 
   * @param integer $menu_tid
   * @return array
   */
  public function getTbMenuContentItems($menu_tid);
  
  /**
   * Return the the hashes built upon subitems content.
   * 
   * @param integer $menu_tid
   * @return array
   */
  public function getTbMenuContentHashes($menu_tid);
  
  /**
   * Return data for client side resolve node id.
   * 
   * @param string $key
   * @return array or FALSE
   *  Array provides key, function, provider 
   */
  public function getJsDataResolvePathNode($key);
  
}
