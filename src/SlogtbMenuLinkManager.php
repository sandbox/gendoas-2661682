<?php

/**
 * @file
 * Contains \Drupal\slogtb\SlogtbMenuLinkManager.
 */

namespace Drupal\slogtb;

use Drupal\Core\Menu\MenuLinkManager;
use Drupal\slogtb\SlogtbMenuTreeStorage;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;


/**
 * Use MenuLinkManager but with own storage and without definitions.
 * 
 */
class SlogtbMenuLinkManager extends MenuLinkManager {
  
  /**
   * There are no Definitions.
   * 
   * @return empty array
   */
  public function getDefinitions() {
    return [];
  }

}
