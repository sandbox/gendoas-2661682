<?php

/**
 * @file
 * Contains \Drupal\slogtb\Event\SlogtbEventSubscriber.
 */

namespace Drupal\slogtb\Event;

use Drupal\slogtb\SlogTb;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogtx\Event\SlogtxEvents;
use Drupal\slogxt\Event\SlogxtEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Cache\Cache;

/**
 * Slog toolbar event subscriber.
 */
class SlogtbEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    // Filter block list: hide slogtb system menu blocks
    // see Drupal\Core\EventSubscriber\MainContentViewSubscriber::onViewRenderArray()
    $events[KernelEvents::VIEW][] = ['onViewRenderArray', 9999];

    //
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 9999];

    // slogtb provides a dummy handler.
    // for real functionality you have to subscribe your own handler.
    // to prefer your handler, set a higher priority (low value)
    $events[SlogtbEvents::REGISTER_SLOGTB_HANDLER][] = ['onRegisterSlogtbHandler', 9999];

    //
    //
    $events[SlogxtEvents::SLOGXT_HAS_MAIN_EDIT_PERM][] = ['hasMainEditOtherPerm', 9999];

    //
    //
    $events[SlogxtEvents::REGISTER_SLOGXT_HANDLER][] = ['onRegisterSlogxtHandler', 0];

    //
    //
    $events[SlogxtEvents::ACTION_GET_APPLY_ALTER][] = ['onActionGetApplyAlter', 0];

    //
    //
    $events[SlogtxEvents::TX_TOOLBAR_CHANGED][] = ['onTxToolbarChanged', 0];


    return $events;
  }

  /**
   * Filter block list: hide slogtb system menu blocks.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent $event
   *   The event to process.
   */
  public function onViewRenderArray(GetResponseForControllerResultEvent $event) {
    $request = $event->getRequest();
    $result = $event->getControllerResult();

    if (isset($result['blocks']) && isset($result['blocks']['#rows'])) {
      $changed = FALSE;
      $rows = $result['blocks']['#rows'];
      foreach ($rows as $key => $row) {
        if ($this->isSysToolbarMenu($row['category']['data'], $row['title']['data'])) {
          unset($rows[$key]);
          $changed = TRUE;
        }
      }

      if ($changed) {
        $result['blocks']['#rows'] = array_values($rows);
        $event->setControllerResult($result);
      }
    }
  }

  public function onKernelResponse(Event $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse && !SlogXt::isAdminRoute()) {
      $to_add['drupalSettings']['slogtb'] = $this->_getHtmlAttachments();
      $response->addAttachments($to_add);
    }
  }

  private function _getHtmlAttachments() {
    $slogtb_handler = SlogTb::getSlogtbHandler();
    $attachments = [
        'handlerProvider' => $slogtb_handler->getProvider(),
        'contentTarget' => $slogtb_handler->getContentTarget(),
        'pathPrefix' => $slogtb_handler->getPathPrefix(),
        'toolbartabs' => SlogXt::getXtSysToolbartabs(),
        'toolbarsRegion' => SlogTb::getJsRegionData(),
        'hasEditableToolbar' => SlogTb::hasEditableToolbar(),
    ];

    return $attachments;
  }

  /**
   * Check data for being slog toolbar system menu
   * 
   * @param array $cdata Category data
   * @param array $tdata Title data
   * @return boolean
   *  TRUE if slog toolbar system menu
   */
  private function isSysToolbarMenu($cdata, $tdata) {
    if (isset($cdata) && $cdata === SlogTb::ADMIN_TB_CATEGORY) {
      if ($label = $tdata['#context']['label']) {
        $parts = explode(SlogTb::ADMIN_TB_SEPARATOR, $label);
        if (count($parts) === 2 && $parts[0] === SlogTb::ADMIN_TB_MENU && $parts[1][0] === '_') {
          return (strpos($parts[1], SlogTx::TOOLBAR_ID_SYS) === FALSE);
        }
      }
    }
    return FALSE;
  }

  /**
   * Set the slogtb dummy handler in $event->slogtbHandlerClass.
   * 
   * For your own handler:
   *  - Set the class as string
   *  - The class has to implement Drupal\slogtb\Handler\SlogtbHandlerInterface
   */
  public function onRegisterSlogtbHandler(Event $event) {
    $event->slogtbHandlerClass = 'Drupal\slogtb\Handler\SlogtbDummyHandler';
  }

  /**
   * Set the slog toolbar handler in $event->slogtbHandlerClass.
   * 
   * - Set the class as string
   * - The class has to implement Drupal\slogtb\Handler\SlogtbHandlerInterface
   */
  public function onRegisterSlogxtHandler(Event $event) {
    $event->slogxtHandlerClass = 'Drupal\slogtb\Handler\SlogtbXtHandler';
  }

  /**
   * 
   * @param Event $event
   */
  public function hasMainEditOtherPerm(Event $event) {
    $data = $event->getData();
// ???? hasTbmenuEditOtherPerm: parameter $plugin_id is required 
//    if (SlogTb::hasEditableToolbar() || SlogTb::hasTbmenuEditOtherPerm()) {
    if (SlogTb::hasEditableToolbar()) {
      $data['result']['has_perm'] = TRUE;
    }
  }

  /**
   */
  public function onActionGetApplyAlter(Event $event) {
    $affected = [
        'slogxt_xtmain_trash',
        'slogxt_xtmain_archive',
    ];
    $data = $event->getData();
    if (in_array($data['plugin_id'], $affected)) {
      $can_apply = & $data['can_apply'];

      $single = !empty($can_apply['single']) ? $can_apply['single'] . ',' : '';
      $my_single = implode(',', [
          'slogtb_edit_tbmenu_new',
          'slogtb_edit_tbmenu_edit',
          'slogtb_edit_tbmenu_rearrange',
          'slogtb_edit_tbtab_edit',
      ]);
      $source = !empty($can_apply['source']) ? $can_apply['source'] . ',' : '';
      $target = !empty($can_apply['target']) ? $can_apply['target'] . ',' : '';

      $can_apply['single'] = $single . $my_single;
      $can_apply['source'] = $source . 'slogtb_edit_tbmenu_move';
      $can_apply['target'] = $target . 'slogtb_edit_tbmenu_move';
    }
  }

  /**
   */
  public function onTxToolbarChanged(Event $event) {
    $toolbars = $event->getEntities();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      Cache::invalidateTags(['config:block.block.stb_toolbar_' . $toolbar_id]);
    }
  }

}
