<?php

/**
 * @file
 * Contains \Drupal\slogtb\Event\SlogtbEvents.
 */

namespace Drupal\slogtb\Event;

/**
 * Defines events thrown by slogtb.
 */
final class SlogtbEvents {

  /**
   * Event for retrieving a hander, which handles events
   * slogtb can not handle by itself.
   */
  const REGISTER_SLOGTB_HANDLER = 'slogtb.register_slogtb_handler';
  
  /**
   * 
   */
  const SLOGTB_MENU_FORM_ALTER = 'slogtb.menu_form_alter';

  /**
   * 
   */
  const SLOGTB_HAS_TBMENU_EDIT_PERM = 'slogtb.has_tbmenu_edit_perm';

}
