<?php

/**
 * @file
 * Contains \Drupal\slogtb\Plugin\slogxt\SxtAction\XtMain\MenuCreate.
 */

namespace Drupal\slogtb\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogtb_xtmain_menu_create",
 *   title = @Translation("New toolbar item"),
 *   menu = "main_dropbutton",
 *   path = "menucreate",
 *   cssClass = "icon-menu-create",
 *   xtProvider = "slogtb",
 *   notop = TRUE,
 *   weight = 20000
 * )
 */
class MenuCreate extends SxtActionPluginBase {
    //todo::actions::main

  public function access() {
    return \Drupal::currentUser()->isAuthenticated();
  }


}
