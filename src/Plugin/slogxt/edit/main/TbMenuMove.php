<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbMenuMove.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbmenu_move",
 *   bundle = "main",
 *   title = @Translation("Move menu item"),
 *   description = @Translation("Select a menu item and move it."),
 *   route_name = "slogtb.edit.main.tbmenu.move",
 *   resolve_base_command = "slogtb::resolvePathTbMenuTid",
 *   resolve_next_command = "slogtb::resolvePathTbTabId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "move sxtrole-menu"
 *   },
 *   skipable = false,
 *   weight = 3
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbMenuMove extends TbPluginEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtTitle' => t('What to move'),
        'xtInfo' => t('Select the menu item you want to move.'),
        'isSource' => TRUE,
      ],
      '{next_entity_id}' => [
        'xtTitle' => t('Where to move'),
        'xtInfo' => t('Select where to move the menu item.'),
        'isTarget' => TRUE,
        'isMoveTarget' => TRUE,
        'preparableToolbars' => SlogTb::getJsPreparableToolbars(),
        'prepLabel' => t('New toolbar item'),
        'prepInfo' => t('Move selected menu item to a newly created toolbar item.'),
      ],
    ];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return $root_term->hasChildren();    
  }
  
  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('move sxtrole-menu') //
        || $this->hasGlobalTbAdmin() //
        );
  }

}
