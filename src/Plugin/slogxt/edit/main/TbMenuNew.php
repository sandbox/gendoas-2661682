<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbMenuNew.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbmenu_new",
 *   bundle = "main",
 *   title = @Translation("New menu item"),
 *   description = @Translation("Add a new menu item beside the selected menu item."),
 *   route_name = "slogtb.edit.main.tbmenu.new",
 *   resolve_base_command = "slogtb::resolvePathTbTabId",
 *   resolve_args = {
 *     "{base_entity_id}" = {
 *       "xtTitle" = @Translation("Where to position"),
 *       "xtInfo" = @Translation("Select the toolbar item where to position the new menu item."),
 *     },
 *   },
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "new sxtrole-menu"
 *   },
 *   skipable = false,
 *   weight = 1
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbMenuNew extends TbPluginEditBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    //todo::current::permissions::user - constraints
    // not here, show page with permissions
    $this->configuration['permissions']['user'] = TRUE;
  }

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtTitle' => t('Where to position'),
        'xtInfo' => t('Select the toolbar item where to position the new menu item.'),
      ],
    ];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('new sxtrole-menu') //
        || $this->hasGlobalTbAdmin() //
        || $this->hasOtherTbPerm() //
        );
  }

}
