<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbMenuEdit.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbmenu_edit",
 *   bundle = "main",
 *   title = @Translation("Edit menu item"),
 *   description = @Translation("Select a menu item and edit it."),
 *   route_name = "slogtb.edit.main.tbmenu.edit",
 *   resolve_base_command = "slogtb::resolvePathTbMenuTid",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "edit sxtrole-menu"
 *   },
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbMenuEdit extends TbPluginEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtInfo' => t('Select the menu item you want to edit.'),
      ],
    ];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return $root_term->hasChildren();
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('edit sxtrole-menu') //
        || $this->hasGlobalTbAdmin() //
        || $this->hasOtherTbPerm() //
        );
  }

}
