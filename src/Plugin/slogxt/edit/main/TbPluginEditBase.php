<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbPluginEditBase.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Interfaces\TxTermInterface;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 */
abstract class TbPluginEditBase extends XtPluginEditBase {

  abstract public function hasActionItemsForSysTab(TxTermInterface $root_term);

  /**
   * Whether action is executable.
   * 
   * This is called by $this->access().
   * 
   * @return boolean
   */
  abstract protected function isActionExecutable();

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->account = \Drupal::currentUser();
    $this->default_role = $this->getUserDefaultRole();
  }

  protected function access() {
    if (SlogXt::isSuperUser($this->account)) {
      return TRUE;
    }

    return ($this->account->isAuthenticated() //
        && SlogTb::hasEditableToolbar() //
        && $this->isActionExecutable());
  }

  protected function getToolbarsVisibleGlobal() {
    return SlogTx::getToolbarsVisibleGlobal();
  }

  /**
   * 
   * @return boolean
   */
  protected function hasGlobalTbAdmin() {
    return SlogTb::hasGlobalTbAdmin($this->account);
  }

  /**
   * 
   * @param string $action_perm
   * @return boolean
   */
  protected function hasRoleTbPerm($action_perm) {
    return (SlogTb::isToolbarVisible('role') && SlogXt::hasPermToolbar('role', ['role' => $action_perm]));
  }

  /**
   * 
   * @return boolean
   */
  protected function hasUserTbMenu() {
    return SlogTb::isToolbarVisible('user');
  }

  /**
   * 
   * @return boolean
   */
  protected function hasOtherTbPerm() {
    return SlogTb::hasTbmenuEditOtherPerm($this->getPluginId());
  }

}
