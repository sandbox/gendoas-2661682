<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbMenuRearrange.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbmenu_rearrange",
 *   bundle = "main",
 *   title = @Translation("Rearrange menu items"),
 *   description = @Translation("Select a subjext and rearrange the menu items."),
 *   route_name = "slogtb.edit.main.tbmenu.rearrange",
 *   resolve_base_command = "slogtb::resolvePathTbTabId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "rearrange sxtrole-menu"
 *   },
 *   skipable = false,
 *   weight = 2
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbMenuRearrange extends TbPluginEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtInfo' => t('Select the toolbar item for which to rearrange menu items.'),
      ],
    ];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return $root_term->hasChildren();    
  }
  
  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('rearrange sxtrole-menu') //
        || $this->hasGlobalTbAdmin() //
        || $this->hasOtherTbPerm() //
        );
  }

}
