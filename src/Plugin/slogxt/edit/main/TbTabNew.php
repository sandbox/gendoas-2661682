<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbTabNew.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtb\SlogTb;
use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbtab_new",
 *   bundle = "main",
 *   title = @Translation("New toolbar item"),
 *   description = @Translation("Select a preparable toolbar and add a new toolbar item."),
 *   route_name = "slogtb.edit.main.tbtab.new",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "new sxtrole-tbtab"
 *   },
 *   skipable = false,
 *   weight = 21
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbTabNew extends TbPluginEditBase {

  protected function getResolveData() {
    return [];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    $toolbars = SlogTb::getToolbarsPreparable();

    // user
    if (isset($toolbars['user'])) {
      return TRUE;
    }
    
    // role
    if (isset($toolbars['role']) && $this->hasRoleTbPerm('new sxtrole-tbtab')) {
      return TRUE;
    }

    // global toolbars
    $vg_tbs = $this->getToolbarsVisibleGlobal();
    $global_tbs = array_intersect_key($toolbars, $vg_tbs);
    if (!empty($global_tbs)) {
      if ($this->account->hasPermission('administer slog taxonomy')) {
        return TRUE;
      }
      else {
        foreach ($global_tbs as $toolbar_id => $toolbar) {
          if ($this->account->hasPermission("administer toolbar $toolbar_id")) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

}
