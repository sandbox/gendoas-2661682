<?php

/**
 * @file
 * Definition of Drupal\slogtb\Plugin\slogxt\edit\main\TbMenuEdit.
 */

namespace Drupal\slogtb\Plugin\slogxt\edit\main;

use Drupal\slogtx\Interfaces\TxTermInterface;

/**
 * @SlogxtEdit(
 *   id = "slogtb_edit_tbtab_edit",
 *   bundle = "main",
 *   title = @Translation("Edit toolbar item"),
 *   description = @Translation("Select a toolbar item and edit it."),
 *   route_name = "slogtb.edit.main.tbtab.edit",
 *   resolve_base_command = "slogtb::resolvePathTbTabId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "edit sxtrole-tbtab"
 *   },
 *   skipable = false,
 *   weight = 20
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbTabEdit extends TbPluginEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtInfo' => t('Select the toolbar item you want to edit.'),
      ],
    ];
  }

  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return !$root_term->getVocabulary()->isUnderscoreToolbar();
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
//    return TRUE;
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('edit sxtrole-tbtab') //
        || $this->hasGlobalTbAdmin() //
        );
  }

}
