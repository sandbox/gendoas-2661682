<?php

/**
 * @file
 * Contains \Drupal\slogtb\Plugin\Derivative\TbContentBlock.
 */

namespace Drupal\slogtb\Plugin\Derivative;

use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides block plugin definitions for all Slog items blocks.
 * 
 * There is a slog item block for each slog toolbar.
 *
 * @see \Drupal\slogtb\Plugin\Block\TbContentBlock
 */
class TbContentBlock extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $admin_label = SlogTb::ADMIN_TB_CONTENT;
    $delimiter = SlogTb::ADMIN_TB_SEPARATOR;
    foreach (SlogTx::getToolbars() as $toolbar_id => $toolbar) {
      $tb_name = $toolbar->label();
      $this->derivatives[$toolbar_id] = [
        'toolbar_id' => $toolbar_id,
        'admin_label' => "{$admin_label}{$delimiter}{$tb_name}",
      ];
      $this->derivatives[$toolbar_id] += $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
