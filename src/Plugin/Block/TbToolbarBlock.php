<?php

/**
 * @file
 * Contains \Drupal\slogtb\Plugin\Block\TbToolbarBlock.
 */

namespace Drupal\slogtb\Plugin\Block;

use Drupal\slogtb\SlogTb;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\slogtx\Interfaces\TxToolbarInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a generic slog toolbar block.
 *
 * @Block(
 *   id = "stb_toolbar",
 *   category = "SlogTb",
 *   deriver = "Drupal\slogtb\Plugin\Derivative\TbToolbarBlock"
 * )
 */
class TbToolbarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $ids = $this->getCurrentRootTermIDs();
    return ['slogtx_tbc:' . $ids];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    $ids = $this->getCurrentRootTermIDs();
    return ["slogtx_tbt.$toolbar_id.$ids"];
  }

  /**
   * Return current root term ids as separated string.
   * 
   * @return string
   */
  private function getCurrentRootTermIDs() {
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    $toolbar = SlogTb::getToolbar($toolbar_id);
    $ids = $toolbar->getCurrentRootTermIDs();
    return implode('-', $ids);
  }

  /**
   * {@inheritdoc}
   * 
   * Called by BlockViewBuilder::preRender()
   */
  public function build() {
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    $toolbar = SlogTb::getToolbar($toolbar_id);
    if (!empty($toolbar) && $toolbar instanceof TxToolbarInterface) {
      if ($toolbar->isSysToolbar() //
          || $toolbar->isNodeSubsysToolbar() //
          || $toolbar->hasVisibleVocabularies()) {
        $build = [
          '#type' => 'slogtb',
          '#toolbar' => $toolbar,
          '#attributes' => [
            'class' => ["stb-toolbar"],
            'toolbar' => $toolbar_id,
          ],
          '#access' => TRUE,
        ];
        return $build;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineNameSuggestion() {
    return strtr($this->pluginId, ':', '__');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    $toolbar = SlogTb::getToolbar($toolbar_id);
    if (empty($toolbar) || !$toolbar->isEnabled()) {
      return AccessResult::forbidden();
    }
    return parent::blockAccess($account);
  }

}
