<?php

/**
 * @file
 * Contains \Drupal\slogtb\Plugin\Block\TbContentDefaultBlock.
 */

namespace Drupal\slogtb\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for toolbar content target.
 *
 * @Block(
 *   id = "slogtb_content_default",
 *   admin_label = @Translation("SlogTb content default"),
 *   category = "SlogTb",
 * )
 */
class TbContentDefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return ['#markup' => 'TbDefaultContentTarget'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE] + parent::defaultConfiguration();
  }

}
