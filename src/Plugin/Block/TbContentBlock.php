<?php

/**
 * @file
 * Contains \Drupal\slogtb\Plugin\Block\TbContentBlock.
 */

namespace Drupal\slogtb\Plugin\Block;

use Drupal\slogtb\SlogTb;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a generic slogitem block.
 *
 * @Block(
 *   id = "stb_content",
 *   category = "SlogTb",
 *   deriver = "Drupal\slogtb\Plugin\Derivative\TbContentBlock"
 * )
 */
class TbContentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   * 
   * Called by BlockViewBuilder::preRender()
   * 
   */
  public function build() {
    // no visible content
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    return [
        '#type' => 'hidden',
        '#value' => $this->pluginId,
        '#attributes' => [
            'class' => ["stb-content"],
            'toolbar' => $toolbar_id,
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineNameSuggestion() {
    return strtr($this->pluginId, ':', '__');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $toolbar_id = $this->getPluginDefinition()['toolbar_id'];
    $toolbar = SlogTb::getToolbar($toolbar_id);
    if (empty($toolbar) || !$toolbar->isEnabled()) {
      return AccessResult::forbidden();
    }
    return parent::blockAccess($account);
  }

}
