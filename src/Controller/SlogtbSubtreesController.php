<?php

/**
 * @file
 * Contains \Drupal\slogtb\Controller\SlogtbSubtreesController.
 */

namespace Drupal\slogtb\Controller;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;


/**
 * Defines a controller for the toolbar module.
 */
class SlogtbSubtreesController extends AjaxDefaultControllerBase {

  
  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $toolbar_id = $request->get('toolbar');
    $toolbartab = $request->get('toolbartab');
    $targetterm = $request->get('targetterm');
    $rootterm_name = $request->get('rootterm');

    // prepare and render subtrees
    $menu_tree = \Drupal::service('slogtb.menu.link_tree');
    $vid = SlogTx::getVocabularyIdFromParts([$toolbar_id, $toolbartab]);
    $vocabulary = SlogTx::getVocabulary($vid);
    $vocabulary->setCurrentTargetTermName($targetterm);
    $subtrees = $menu_tree->getRenderedSubtrees($vid, $rootterm_name);
    if (!empty($subtrees)) {
      $subtrees['menuHashes'] = $subtrees['#menuhashes'];
      unset($subtrees['#menuhashes']);
      unset($subtrees['#cache']);
    }
    $data = [
        'toolbar' => $toolbar_id,
        'toolbartab' => $toolbartab,
        'rootterm' => $rootterm_name,
        'subtrees' => $subtrees ?: FALSE,
        'uid' => \Drupal::currentUser()->id(),
    ];
    
    return $data;
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxDefaultControllerBase::responseAlter()
   */
  protected function responseAlter($response) {
    //this is from toolbar module:
    // The Expires HTTP header is the heart of the client-side HTTP caching. The
    // additional server-side page cache only takes effect when the client
    // accesses the callback URL again (e.g., after clearing the browser cache
    // or when force-reloading a Drupal page).
    $max_age = 365 * 24 * 60 * 60;
    $response->setPrivate();
    $response->setMaxAge($max_age);

    $expires = new \DateTime();
    $expires->setTimestamp(REQUEST_TIME + $max_age);
    $response->setExpires($expires);

    return $response;
  }

}
