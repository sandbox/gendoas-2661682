<?php

/**
 * @file
 * Contains \Drupal\slogtb\Controller\OnTbMenuItemSelect.
 */

namespace Drupal\slogtb\Controller;

use Drupal\slogtx\SlogTx;
use Drupal\Core\Render\Element\StatusMessages;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

class OnTbMenuItemSelect extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $msg_not_implemented = t('Dummy content. Handler not implemented.');
    $this->messenger->addWarning($msg_not_implemented);
    $drupal_msg = StatusMessages::renderMessages('warning');
    $drupal_msg = \Drupal::service('renderer')->render($drupal_msg);

    $request = \Drupal::request();
    $tid = $request->get('tbmenu_tid');
    $menu_term = SlogTx::getMenuTerm($tid);
    $listLabel = $menu_term->label();
    $vocabulary = $menu_term->getVocabulary();
    $vid = $vocabulary->id();
    list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vid);
    $path = "$vid/$tid";
    $path_label = $menu_term->pathLabel();

    $content = $msg_not_implemented . ' <br />'
            . t('TbSee how to ...') . ' <br />'
            . 'Item: ' . $path_label . ' <br />'
            . 'Path: ' . $path;

    $data = [
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
        'tid' => $tid,
        'item' => $path_label,
        'path' => $path,
        'settings' => [],
        'provider' => 'slogtb',
        'content' => $drupal_msg . $content,
    ];
    return $data;
  }

}
