/**
 * @file
 * A Backbone Model for the toolbar.
 */

(function(Backbone, Drupal) {

  "use strict";

  /**
   * Backbone model for the slog toolbar.
   */
  Drupal.slogtb.SlogtbModel = Backbone.Model.extend({
    sxtThisClass: 'slogtb.SlogtbModel',
    defaults: {
      // The active toolbar tab. All other tabs should be inactive under
      // normal circumstances. It will remain active across page loads. The
      // active item is stored as an ID selector e.g. '#toolbar-item--1'.
      activeTab: null,
      // Represents whether a tray is open or not. Stored as an ID selector e.g.
      // '#toolbar-item--1-tray'.
      activeTray: null,
      activeMenuItemPath: null
    }
  });

})(Backbone, Drupal);
