/**
 * @file
 * A Backbone Model for collapsible menus.
 */

(function (Backbone, Drupal) {

  "use strict";

  /**
   * Backbone Model for collapsible menus.
   */
  Drupal.slogtb.SlogtbMenuModel = Backbone.Model.extend({
    sxtThisClass: 'slogtb.SlogtbMenuModel',
    initialize: function(options) {
      this.toolbar = options.toolbar;
      this.toolbartab = options.toolbartab;
      return this;
    },
    defaults: {
      // Menu subtrees are loaded through an AJAX request.
      areSubtreesLoaded: false,
      contentData: null,
      //
      subtrees: {}
    }
  });

})(Backbone, Drupal);
