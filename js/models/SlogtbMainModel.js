/**
 * @file
 * A Backbone Model for the slog toolbars.
 */

(function (Backbone, Drupal) {

  "use strict";

  Drupal.slogtb.SlogtbMainModel = Backbone.Model.extend({
    sxtThisClass: 'slogtb.SlogtbMainModel',
    stbMinWidth: 38,
    /**
     * {@inheritdoc}
     */
    defaults: {
      /**
       * Collect region data on init and allow modules to change them.
       */
      tbRegionData: {},
      /**
       * Keep track of actually active toolbar.
       * 
       * At each time there is one active toolbar (with open tray) or none.
       */
      activeSlogtbView: null,
      /**
       * Temporary for altering trayPlacement data.
       * 
       * Other modules can alter the placement data.
       * @see hookDataAlter('trayPlacement'...) in SlogtbView.adjustTrayPlacement()
       */
      trayPlacement: null,
      /**
       * Temporary for altering staticSettings data.
       * 
       * Other modules can alter the statics data.
       * @see hookDataAlter('staticSettings'...) in SlogtbMainView.alterToolbarStatics()
       */
      staticSettings: null,
      /**
       * On slogtb menu click the content is loading.
       */
      loadingContent: null,
      /**
       * Notify a menu item was edited, with changed data
       */
      editedTbMenu: null,
      /**
       * Notify a menu item were rearranged, with changed data
       */
      rearrangedTbMenu: null,
      /**
       * Notify a menu tab was edited, with changed data
       */
      editedTbTab: null,
      
      /**
       * allow modules actions on role rebuild
       */
      rebuildRoleState: null,

      /**
       * allow modules to react on menu item clicked
       */
      menuItemClicked: null
    },
    /**
     * Give other modules the opportunity to alter data.
     * 
     * @param string key The change key for triggered event  
     * @param mixed data The data to be altered
     * @param {type} keepClean On TRUE clear the temp data silently
     * @returns mixed Altered data
     */
    hookDataAlter: function (key, data, keepClean) {
      this.set(key, data);
      var altered = this.get(key);
      keepClean && this.set(key, null, {silent: true});
      return altered;
    }
  });

})(Backbone, Drupal);
