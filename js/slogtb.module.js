/**
 * @file
 * Defines the behavior of slog toolbars.
 */
(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  Drupal.slogtb = {};
  var _sxt = Drupal.slogxt
          , _stb = Drupal.slogtb;

  /**
   * Registers tabs with the toolbar.
   *
   * The Drupal toolbar allows modules to register top-level tabs. These may point
   * directly to a resource or toggle the visibility of a tray.
   *
   * Modules register tabs with hook_toolbar().
   */
  Drupal.behaviors.slogtb = {
    attachDone: false,
    attach: function (context) {
      // nothing to do if no toolbars
      if (this.attachDone || !drupalSettings.slogtb || !drupalSettings.slogtb.toolbars) {
        return;
      }

      // do attach once only
      this.attachDone = true;
      // do not put main objects into models/views !!!
      // in models/views are toolbars only
      if (!_stb.slogtbMainModel) {
        _stb.slogtbMainModel = new _stb.SlogtbMainModel({});
        _stb.slogtbMainView = new _stb.SlogtbMainView({
          el: $('body').get(0), //???
          model: _stb.slogtbMainModel
        });
      }

      // Prepare tbsubmenues like tbnode.
      // tbnode has no data by default (has no block positioned)
      // and is loaded after a content with own submenu has been loaded
      var tbsubmenues = drupalSettings.slogtb.tbsubmenu || {};
      $.each(tbsubmenues, function (toolbar, tbData) {
        if (!tbData.targetId || !tbData.wrapperId) {
          slogErr(toolbar + ': required block data not get.');
        }

        var $tb = $('#slogtb-' + toolbar)
                , $tbBlock = $tb.parents('.block.block-slogtb').first();
        if (!$tbBlock.length) {
          slogErr(toolbar + ': block element not found.');
          return;
        }

        var $tbWrapper = $(Drupal.theme.slogxtWrapper(tbData.wrapperId));
        $tbWrapper.addClass('slogtb-tabs-iconized');
        $tbWrapper.prepend($tbBlock);
        $('#' + tbData.targetId).prepend($tbWrapper);
      });

      // Create models and views for all regular toolbars
      _stb.slogtbCollection = new Backbone.Collection();
      $.each(drupalSettings.slogtb.toolbars, function (toolbar, toolbarData) {
        if (!_sxt.isTbSysId(toolbar) || _sxt.isUserAuthenticated()) {
          $('#slogtb-' + toolbar).once('slogtb').each(function () {
            _stb.createTbView(toolbar, $(this), toolbarData);
            drupalSettings.slogtb.toolbars[toolbar] = true;
          });
        }
      });
    }
  };




  /**
   * Slog toolbar methods of Backbone objects.
   */
  var initStb = {
    // A hash of View instances.
    views: {},
    // A hash of Model instances.
    models: {},
    elements: {},
    ajax: {},
    overlayTimer: false,
    mainModel: function () {
      return this.slogtbMainModel;
    },
    mainView: function () {
      return this.slogtbMainView;
    },
    createTbView: function (toolbar, $el, toolbarData) {
      var slogtbCollection = this.slogtbCollection
              , tbModels = this.models[toolbar] = {}
      , tbViews = this.views[toolbar] = {}
      // Create the toolbar models and views
      , model = tbModels.slogtbModel = new this.SlogtbModel({
        $el: $el,
        id: toolbar,
        toolbar: toolbar
      });
      slogtbCollection.add(model);
      tbViews.slogtbView = new this.SlogtbView({
        el: $el[0],
        model: model,
        collection: slogtbCollection,
        id: toolbar,
        toolbar: toolbar,
        toolbarData: toolbarData
      });

      return tbViews.slogtbView;
    },
    getVocabId: function (toolbar, toolbartab) {
      return toolbar + '__' + toolbartab;
    },
    getVocabCC: function (toolbar, toolbartab) {
      return toolbar.capitalize() + toolbartab.capitalize();
    },
    getSlogtbModel: function (toolbar) {
      var tbModels = this.models[toolbar];
      return tbModels ? tbModels.slogtbModel || false : false;
    },
    getSlogtbView: function (toolbar) {
      var tbViews = this.views[toolbar];
      return tbViews ? tbViews.slogtbView || false : false;
    },
    hasSlogtbToolbar: function (toolbar) {
      return !!(this.getSlogtbView(toolbar));
    },
    getSlogtbMenuView: function (toolbar, toolbartab) {
      var tbView = this.getSlogtbView(toolbar)
              , childViews = tbView.childViews || {};
      tbView.isTbSys && (toolbartab = 'dummy');
      return childViews ? childViews[toolbartab + 'View'] || false : false;
    },
    isHandlerProvider: function (module) {
      var handlerProvider = drupalSettings.slogtb.handlerProvider || '';
      return (handlerProvider === module);
    },
    settingsContentTarget: function () {
      return drupalSettings.slogtb.contentTarget || '';
    },
    getActiveSlogtbView: function () {
      return this.mainModel().get('activeSlogtbView');
    },
    getActiveToolbarModel: function () {
      return this.getActiveSlogtbView().model;
    },
    closeActiveToolbar: function () {
      // close open popup, includes active tab
      _sxt.closeOpenPopup();
    },
    loadMenuSelectedSys: function (pathData, retrigger) {
      this.loadMenuSelected(pathData, retrigger, _sxt.getTbSysId());
    },
    loadMenuSelected: function (pathData, retrigger, toolbar) {
      toolbar = toolbar || pathData.toolbar;
      var tbView = this.getSlogtbView(toolbar);
      if (tbView) {
        retrigger && tbView.model.set('activeMenuItemPath', {}, {silent: true});
        tbView.model.set('activeMenuItemPath', pathData);
      }
    },
    hasEditableToolbar: function () {
      return drupalSettings.slogtb.hasEditableToolbar;
    },
    hasPermRoleMenu: function (perm) {
      if (_.isBoolean(perm)) {
        return perm;
      }

      if (Drupal.sxt_group.hasPermission('admin sxtrole-menu')) {
        return true;
      }

      return Drupal.sxt_group.hasPermission(perm);
    },
    getPermBaseTbId: function (base_tb) {
      base_tb = base_tb || '__error__';
      var toolbar_id = base_tb
              , parts = base_tb.split(';');
      (parts.length > 1) && (toolbar_id = parts[1]);
      return toolbar_id;
    }
  };
  $.extend(true, _stb, initStb);


  //
  // on document ready
  //
  $(document).ready(function () {
    var mainView = _stb.mainView();
    mainView && mainView.initOnDocumentReady();
  });

})(jQuery, Drupal, drupalSettings, Backbone, _);
