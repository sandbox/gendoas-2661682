/**
 * @file
 * A Backbone view for the toolbar element.
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse, undefined) {

  "use strict";

  var _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt;

  var stbMainViewExtensions = {
    sxtThisClass: 'slogtb.SlogtbMainView',
    /**
     * {@inheritdoc}
     */
    initialize: function (options) {
      this.sxtIsPermanent = true;
      var adminModel = Drupal.toolbar.models.toolbarModel || false;
      if (adminModel) {
        this.listenTo(adminModel, 'change:orientation change:isOriented change:offsets', this.onChangeAdminToolbar);
      }

      // create required objects
      this.slogtbViews = {};  // all toolbars
      this.slogtbRegion = {}; // data for toolbars in region

      $(window).on('resize', $.proxy(this.onWindowResize, this));
      this.listenTo(this.model, 'change:activeSlogtbView', this.onChangeActiveToolbarView);
      this.listenTo(this.model, 'change:editedTbMenu', this.onChangeEditedTbMenu);
      this.listenTo(this.model, 'change:rearrangedTbMenu', this.onChangeRearrangedTbMenu);
      this.listenTo(this.model, 'change:editedTbTab', this.onChangeEditedTbTab);
      this.listenTo(this.model, 'change:movedTbMenu', this.onChangeMovedTbMenu);

      var stateModel = _sxt.stateModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);

      var screenModel = _sxt.screenModel();
      this.listenTo(screenModel, 'change:openPopUp', this.onChangeOpenPopUp);
      return this;
    },
    onChangeOpenPopUp: function (screenModel, openPopUp) {
//      slogLog('SlogtbMainView.onChangeOpenPopUp: ' + openPopUp);
      var aView = _stb.getActiveSlogtbView();
      if (aView && !openPopUp) {
//        slogLog('SlogtbMainView: .......onChangeOpenPopUp.toolbar: ' + aView.id);
        aView.closeActiveTab();
      }
    },
    onChangeStateState: function (stateModel, stateState) {
      var stateData = stateModel.get('stateData');
      try {
        switch (stateState) {
          case 'loaded':
            var tbData = stateData.slogtbMain || false;
            if (tbData) {
              $.each(this.slogtbViews, function (id, tbView) {
                if (!tbView.isTbSys && tbData[id]) {
                  tbView.model.set('activeMenuItemPath', tbData[id]['activeMenuItemPath']);
                }
              });
            }
            break;
          case 'writing':
            var tbData = {};
            $.each(this.slogtbViews, function (id, tbView) {
              tbData[id] = {
                'activeMenuItemPath': tbView.model.get('activeMenuItemPath')
              };
            });
            stateData.slogtbMain = tbData;
            break;
        }
      } catch (e) {
        slogErr('SlogtbMainView.onChangeStateState: ' + stateState + "\n" + e.message);
      }
    },
    onChangeEditedTbMenu: function (model, data) {
      var view = _stb.getSlogtbMenuView(data.toolbar, data.toolbartab)
          , $mEl = view ? view.$el.find('#slogtb-link-' + data.tid) : $();
      $mEl.length && $mEl.text(data.name).attr('title', data.description);
      view && view.addNoCache(data.tid);
    },
    onChangeRearrangedTbMenu: function (model, data) {
      var view = _stb.getSlogtbMenuView(data.toolbar, data.toolbartab);
      view && view.rebuildMenu();
    },
    onChangeEditedTbTab: function (model, data) {
//      slogLog('stbMainViewExtensions..........onChangeEditedTbTab()');
      var mnuView = _stb.getSlogtbMenuView(data.toolbar, data.toolbartab)
          , $tEl = mnuView
          ? mnuView.$el.closest('.toolbar-tab').find('#slogtb-item-' + data.vid) : $()
          , pathLabel = data.pathLabel
          , parts = pathLabel.split('::')
          , tbView = _stb.getSlogtbView(data.toolbar);
      (parts.length > 1) && (pathLabel = parts[1]);
      $tEl.length && $tEl.text(pathLabel).attr('title', data.description);
      tbView && tbView.refreshBlockWidth();
    },
    onChangeMovedTbMenu: function (m, data) {
//      slogLog('SlogtbMainView: ................onChangeMovedTbMenu');
      if (!data.reload) {
        var view;
        if (!_sxt.isTbSysId(data.sToolbar)) {
          view = _stb.getSlogtbMenuView(data.sToolbar, data.sToolbartab);
          view && view.rebuildMenu();
        }
        if (!_sxt.isTbSysId(data.tToolbar)) {
          view = _stb.getSlogtbMenuView(data.tToolbar, data.tToolbartab);
          view && view.rebuildMenu();
        }
      }
    },
    initOnDocumentReady: function () {
//      slogLog('SlogtbMainView: ................initOnDocumentReady');
      this.listenTo(this.model, 'change:tbRegionData', this.onChangeRegionData);
      this.initRegions();
      this.updateRegionData();
      this.alterToolbarStatics();
    },
    initRegions: function () {
      var tbRegionData = this.model.get('tbRegionData')
          , rid, data, $region;
      for (rid in tbRegionData) {
        data = tbRegionData[rid];
        data.iconized = undefined;  // ensure initial triggering change region data
//        slogLog('SlogtbMainView: initRegions: ' + rid + ': ' + data.tabsFullWidth);
        $region = data.$region;
        if (data.toolbarCount > 1) {
          // multiple toolbars, add wrapper
          var cls = 'slogtb-multiple-wrapper block-slogtb'
              , $wrapper = $(_tse('div', {class: cls}))
              , $switch = $(_tse('div', {id: 'slogtb-menu-switch-btn'}));
          $wrapper.append($region.find('.block-slogtb.stb-toolbar'));
          $region.prepend($wrapper);
          $wrapper.before($switch);
        }
        var $mainDropButton = $region.find('#block-slogxtmaindropbutton');
        $mainDropButton.length && $region.prepend($mainDropButton);
      }
    },
    initMenuSwitch: function (rData, rWidth) {
      this.menuSwitch = this.menuSwitch || {};
      if (!this.menuSwitch[rData.regionId]) {
        var $switch = rData.$region.find('#slogtb-menu-switch-btn')
            , items = []
            , maxItems = parseInt((rWidth - 10) / this.model.stbMinWidth, 10) - 1
            , cur = 0
            , number = 0
            , tbId, tbView;
        for (tbId in drupalSettings.slogtb.toolbarsRegion) {
          tbView = rData.tbViews[tbId] || false;
          if (tbId && (tbId[0] !== '_') && tbView) {
            number += tbView.tabsNumber;
            if (number > maxItems) {
              cur++;
              number = tbView.tabsNumber;
            }
            items[cur] = items[cur] || {};
            items[cur][tbView.id] = tbView;
          }
        }

        this.menuSwitch[rData.regionId] = {
          $el: $switch,
          items: items,
          current: -1
        };
        $switch.once('slogtb-mnu-switch').on('click', $.proxy(function (e) {
          this.onMenuSwitchClick(e);
        }, this));
        $switch.click();
      }
      rData.$region.addClass('has-menu-switch');
    },
    resetMenuSwitch: function (rData) {
      rData.$region.removeClass('has-menu-switch');
    },
    onMenuSwitchClick: function (e) {
      var $region = $(e.target).closest('.region')
          , id = $region.first().attr('id')
          , switchData = this.menuSwitch[id] || false
          , current, hide;
      if (switchData && switchData.items) {
        current = switchData.current + 1;
        !switchData.items[current] && (current = 0);
        switchData.current = current;
        $.each(switchData.items, function (idx, tbViews) {
          hide = (idx !== current);
          $.each(tbViews, function (id, tbView) {
            tbView.$el.closest('.stb-toolbar').toggleClass('slogtb-menu-hidden', hide);
          });
        });
      } else {
        slogErr('onMenuSwitchClick: switch data not found.');
      }
    },
    updateRegionData: function () {
//      slogLog('SlogtbMainView: ................updateRegionData');
      var tbRegionData = _.clone(this.model.get('tbRegionData'))
          , rid, data, regionWidth, nOff;
      for (rid in tbRegionData) {
        data = _.clone(tbRegionData[rid]); // deep clone !!
        regionWidth = data.$region.width();
        nOff = data.toolbarCount * 3;
        data.iconized = (regionWidth < data.tabsFullWidth + nOff);
        tbRegionData[rid] = data;
        if (data.toolbarCount > 1) {
          if (data.iconized && regionWidth < data.tabsIconizedWidth + nOff) {
            this.initMenuSwitch(data, regionWidth);
          } else {
            this.resetMenuSwitch(data);
          }
        }
      }
      this.model.set('tbRegionData', tbRegionData)
    },
    alterToolbarStatics: function () {
      // trigger alter staticSettings after views of other modules/themes are created
      var thisModel = this.model
      _.each(this.slogtbViews, function (tbView) {
        if (!tbView.isTbSys) {
          tbView.model.statics = thisModel.hookDataAlter('staticSettings', tbView.model.statics, true);
        }
      });
    },
    onChangeRegionData: function (model, tbRegionData) {
//      slogLog('SlogtbMainView: ................onChangeRegionData');
      var previous = model.previous('tbRegionData')
          , rid, data, prev, minWidth;
      for (rid in tbRegionData) {
        data = tbRegionData[rid];
//        slogLog('SlogtbMainView: onChangeRegionData: ' + rid + ': ' + data.tabsFullWidth);
        prev = previous[rid];
        if (data.iconized !== prev.iconized) {
          // set/unset iconized class
          data.$region.toggleClass('slogtb-tabs-iconized', data.iconized);
          // set min-width for each block
          _.each(data.tbViews, function (view) {
            var st = view.model.statics;
            st.multipleToolbars = (data.toolbarCount > 1);
            minWidth = data.iconized ? st.tabsIconizedWidth : st.tabsFullWidth;
            st.$block.css('min-width', minWidth + 2);
          });
        }
      }
    },
    onChangeActiveToolbarView: function (model, activeTbView) {
//      slogLog('SlogtbMainView: onChangeActiveToolbarView');
      var prevView = model.previous('activeSlogtbView');
      if (prevView) {
        prevView.model.set('activeMenuItemPath', null, {silent: true});
        model.previous('activeSlogtbView').model.set('activeTab', null);
      }
    },
    updateToolbars: function () {
      this.updateRegionData();
      var aView = _stb.getActiveSlogtbView();
      aView && aView.adjustTrayPlacement();
    },
    onChangeAdminToolbar: function () {
      this.updateToolbars();
    },
    onWindowResize: function (e) {
//      slogLog('SlogtbMainView: ................onWindowResize');
      this.regionTimeout && clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout($.proxy(this.updateToolbars, this), 10);
    }
  };

  /**
   * Backbone view for the toolbar element.
   */
  _stb.SlogtbMainView = Backbone.View.extend(stbMainViewExtensions);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
