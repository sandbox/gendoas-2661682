/**
 * @file
 * A Backbone view for the collapsible menus.
 */

(function ($, Backbone, Drupal, drupalSettings, _, undefined) {

  "use strict";

  var _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt;

  var stbMenuViewExt = {
    sxtThisClass: 'slogtb.SlogtbMenuView',
    initialize: function (options) {
      this.noCache = {};
      this.stateModel = _sxt.stateModel();
      this.mainModel = _stb.mainModel();
      this.slogtbView = options.slogtbView;
      this.slogtbModel = this.slogtbView.model;
      this.toolbar = this.slogtbView.toolbar;
      this.toolbartab = options.toolbartab;
      this.$tbTab = this.$el.closest('.toolbar-tab');
      this.refreshEvent = 'slogtbMenu' + _stb.getVocabCC(this.toolbar, this.toolbartab);
      drupalSettings.slogtb.toolbartabs = drupalSettings.slogtb.toolbartabs || {};
      drupalSettings.slogtb.toolbartabs[this.toolbartab] = this.toolbar;

      this.listenTo(this.model, 'change:subtrees', this.changedSubtrees);
      this.listenTo(this.slogtbModel, 'change:activeMenuItemPath', this.onChangeTbActiveMenuItemPath);

      // dom fix
      this.markListLevels(this.$el.find('ul.toolbar-menu'));
      // attach
      this.attach();

      return this;
    },
    attach: function () {
      this.$el.find('a.slogtb-link-item')
          .once('slogtb-link')
          .on('click', $.proxy(function (event) {
            this.onMenuItemClick(event);
          }, this));
    },
    detach: function () {
      this.$el.removeOnce('slogtb-link').off();
    },
    markListLevels: function ($lists, level) {
      level = (!level) ? 1 : level;
      var $lis = $lists.children('li').addClass('level-' + level);
      $lists = $lis.children('ul');
      if ($lists.length) {
        this.markListLevels($lists, level + 1);
      }
    },
    changedSubtrees: function (model, subtrees) {
      // remember subtrees are loaded.
      model.set('areSubtreesLoaded', true);
      // add menuHashes to toolbarData
      if (subtrees.menuHashes) {
        var toolbarData = this.slogtbModel.get('toolbarData');
        _.extend(toolbarData.menuHashes, subtrees.menuHashes);
      }

      // detach
      this.detach();
      // Add subtrees.
      for (var id in subtrees) {
        if (subtrees.hasOwnProperty(id)) {
          this.$el.find('#slogtb-link-' + id).after(subtrees[id]);
        }
      }
      // dom fix
      this.markListLevels(this.$el.find('ul.toolbar-menu'));
      // attach
      this.attach();

      // Render the main menu as a nested, collapsible accordion.
      if ('drupalToolbarMenu' in $.fn) {
        this.$el
            .children('.toolbar-menu')
            .drupalToolbarMenu();
      }
    },
    onMenuItemClick: function (e) {
//      slogLog('SlogtbMenuView: ..............onMenuItemClick');
      e.preventDefault();
      e.stopPropagation();

      var $element = $(e.target)
          , entityId = $element.attr('entityid')
          , path = $element.data('drupalLinkSystemPath') || ''
          , data = {
            path: path,
            tid: entityId,
            toolbar: this.toolbar,
            toolbartab: this.toolbartab
          };
      this.slogtbView.closeActiveTab();
      // Allow modules to react on menu item clicked
      this.mainModel.slogxtSetAndTrigger('change:menuItemClicked', null, this);
      this.slogtbModel.set('activeMenuItemPath', data);

      if (_sxt.isDoNavigateActive()) {
        var donData = {
          menuItemPath: path,
          toolbar: this.toolbar,
          toolbartab: this.toolbartab,
          targetKey: 'SlogtbMenuItem',
          noServerResolve: true,
          entityId: entityId,
          rootTermId: this.model.get('rootTermId'),
          label: $element.text(),
          info: $element.attr('title')
        };
        _sxt.setDoNavigateRawData(donData);
      }
    },
    onChangeTbActiveMenuItemPath: function (tbm, pathData) {
//      slogLog('SlogtbMenuView: ..............onChangeTbActiveMenuItemPath');
      (_sxt.isTbSysId(this.toolbar) || this.toolbartab === pathData.toolbartab) //
          && this.loadContent(pathData);
    },
    activateTblineTab: function () {
      var mainModel = this.slogtbView.mainModel
          , data = {
            path: this.curPath,
            toolbar: this.toolbar,
            toolbartab: this.toolbartab
          };
      mainModel.set('loadingContent', data);
      mainModel.set('loadingContent', null, {silent: true});
    },
    addNoCache: function (tid) {
      this.noCache[tid] = true;
      (this.curTid == tid) && this.slogtbModel.slogxtReTrigger('change:activeMenuItemPath');
    },
    getAjaxRebuild: function () {
      if (!this.ajaxRebuild) {
        this.ajaxRebuild = new _sxt.ajaxCreate(this.refreshEvent, this.$el);
      }
      return this.ajaxRebuild;
    },
    rebuildMenu: function () {
      !this.slogtbView.isTbSys && !this.slogtbView.getTargetEntityId() && this.loadMenu();
    },
    loadMenu: function () {
      if (this.slogtbView.isTbSys) {
        return;
      }
      this.detach();
      this.model.set('subtrees', {}, {silent: true});
      this.model.set('areSubtreesLoaded', false, {silent: true});
      this.$el.closest('toolbar-tab')
          .find('>a.toolbar-item')
          .data('drupal-subtrees', undefined)

      var vid = _stb.getVocabId(this.toolbar, this.toolbartab)
          , tid = this.model.get('rootTermId')
          , path = _sxt.baseAjaxPath('slogtb') + 'refresh/tbmenu/' + vid + '/' + tid
          , ajax = this.getAjaxRebuild();
      ajax.url = ajax.options.url = Drupal.url(path);
      ajax.slogxt.targetThis = this;
      ajax.slogxt.targetCallback = this.ajaxRebuildResponse;
      ajax.slogxt.vid = vid;
      this.$el.empty();

      // ajax call by triggering an event
      ajax.$xtelement.trigger(this.refreshEvent);
    },
    ajaxRebuildResponse: function (ajax, response, status) {
      var data = response.data || {};
      if (data.html && data.selector && data.tbdMenutabs) {
        var html = $(data.html).find(data.selector).html()
            , tbData = this.slogtbModel.get('toolbarData')
            , aView = _stb.getActiveSlogtbView()
            , aTab = aView ? aView.model.get('activeTab') : false
            , aId = aTab ? aTab.id || false : false;
        tbData.menutabs[this.toolbartab] = data.tbdMenutabs;

        if (aId && aId === 'slogtb-item-' + ajax.slogxt.vid) {
          _sxt.closeOpenPopup();
        }

        this.$el.html(html || '');
        this.model.set('areSubtreesLoaded', false, {silent: true});
        // dom fix
        this.markListLevels(this.$el.find('ul.toolbar-menu'));
        //attach
        this.attach();
      } else if (status === 'success') {
        this.$el.empty();
      } else {
        slogErr(data.message || 'Unknown error.');
      }
    },
    loadContent: function (pathData, reset) {
      this.curPath = '';
      this.curTid = '';
      var path = pathData.path || false
          , tid = pathData.tid || false;
//      slogLog('SlogtbMenuView.........loadContent: ' + path);
      if (!!path && !!tid) {
        this.curPath = path;
        this.curTid = tid;
        this.activateTblineTab();
        var storageKey = _sxt.storageKey('StbMenu', path)
            , data = false
            , tbData = this.slogtbModel.get('toolbarData')
            , tidHash = !!tbData.menuHashes ? tbData.menuHashes[tid] || false : false
            , keyHash = _sxt.storageKey('StbMenu', path, 'hash')
            , cachedHash = localStorage.getItem(keyHash);
        (!cachedHash || cachedHash != tidHash) && (reset = true);
        if (this.noCache[tid]) {
          reset = true;
          delete this.noCache[tid];
        }
        if (reset) {
          sessionStorage.removeItem(storageKey);
        } else {
          data = JSON.parse(sessionStorage.getItem(storageKey));
        }

        if (data && !!data.content) {
          this.setContentData(data);
        } else {
          localStorage.removeItem(keyHash);
          var st = this.slogtbView.model.statics
              , h = _sxt.helper
              , path = pathData.path || false
              , pathPrefix = drupalSettings.slogtb.pathPrefix
              , pdata = h.splitPath(path, pathPrefix, ['selected'])
              , ajax = this.slogtbView.getAjaxSelect();
          if (pdata.selected && ajax) {
            ajax.url = ajax.options.url = Drupal.url(path);
            ajax.options.data.tbmenu_tid = pdata.selected;
            ajax.slogxt.targetThis = this;
            ajax.slogxt.targetCallback = this.ajaxResponse;
            ajax.slogxt.path = this.curPath;
            ajax.slogxt.storageKey = storageKey;
            ajax.slogxt.keyHash = keyHash;

            // hide element
            // unbind events later (in onChangeSlogitemList)
            st.$contentTarget.hide(0);

            // ajax call by triggering an event
            ajax.$xtelement.trigger(this.slogtbView.selectEvent);
          }
        }
      }
    },
    ajaxResponse: function (ajax, response, status) {
      var data = response.data
          , tid = data.tid
          , keyHash = ajax.slogxt.keyHash
          , tidHash = data.settings.tidHash
          , tbData = this.slogtbModel.get('toolbarData');
      if (!!tidHash) {
        localStorage.setItem(keyHash, tidHash);
        tbData.menuHashes = tbData.menuHashes || {};
        tbData.menuHashes[tid] = tidHash;
      }
      data.path = ajax.slogxt.path;
      sessionStorage.setItem(ajax.slogxt.storageKey, JSON.stringify(data));
      this.setContentData(data);
    },
    setContentData: function (content) {
      this.slogtbView.model.set('contentData', content);
    }
  };

  /**
   * Backbone View for collapsible menus.
   */
  _stb.SlogtbMenuView = Backbone.View.extend(stbMenuViewExt);

})(jQuery, Backbone, Drupal, drupalSettings, _);
