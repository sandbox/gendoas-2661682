/**
 * @file
 * A Backbone view for the toolbar element.
 */

(function ($, Drupal, drupalSettings, Backbone, localStorage, _, undefined) {

  "use strict";

  var _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt;

  var stbVisualViewExtensions = {
    sxtThisClass: 'slogtb.SlogtbView',
    /**
     * {@inheritdoc}
     */
    initialize: function (options) {
      var toolbarData = options.toolbarData || {};
      this.hasMenus = !!toolbarData.hasMenus;
      this.toolbar = options.toolbar;
      this.hasRoleTab = toolbarData.hasRoleTab || false;
      this.refreshEvent = 'slogtbToolbar' + this.toolbar.capitalize();
      this.isTbSys = _sxt.isTbSysId(this.toolbar);

      this.model.set('toolbarData', toolbarData);
      this.$el.off('slogtbSelect');
      this.ajaxSubtrees = new _sxt.ajaxCreate('slogtbSubtrees', this.$el, true);
      this.selectEvent = 'slogtbSelect' + (this.toolbar).capitalize();

      this.mainModel = _stb.mainModel();
      this.mainView = _stb.mainView();
      this.mainView.slogtbViews[this.toolbar] = this;

      this.childViews = {};
      this.initMenues(toolbarData);
      this.initStatics(toolbarData);
      this.initDOM();
      this.initIcon();
      this.attach();
      this.showToolbar(this.hasMenus);

      this.listenTo(this.model, 'change:activeTab', this.onChangeActiveTab);
      this.listenTo(this.model, 'change:activeTray', this.onChangeActiveTray);
      this.listenTo(this.model, 'change:contentData', this.onChangeContentData);
      this.listenTo(this.model, 'change:menuBlockData', this.onChangeMenuBlockData);

      return this;
    },
    attach: function () {
      this.$el.find('.toolbar-bar .toolbar-tab')
          .once('stb-ajax')
          .on('click', $.proxy(this.onTabClick, this));
      this.$el.find('.toolbar-bar .toolbar-tray')
          .once('stb-ajax')
          .on('click', $.proxy(this.onTrayClick, this));
    },
    detach: function () {
      this.stopListening(this.model);
      this.$el.find('.toolbar-bar .toolbar-tab, .toolbar-bar .toolbar-tray')
          .removeOnce('stb-ajax').off();
      this.ajaxSubtrees.$xtelement.off();
      this.ajaxSelect && this.ajaxSelect.$xtelement && this.ajaxSelect.$xtelement.off();
      delete this.mainView.slogtbViews[this.toolbar];
      this.showToolbar(false);
    },
    getAjaxSelect: function () {
      if (!this.ajaxSelect) {
        var st = this.model.statics;
        this.ajaxSelect = new _sxt.ajaxCreate(this.selectEvent, st.$contentTarget);
      }
      return this.ajaxSelect;
    },
    initMenues: function (toolbarData) {
      if (this.hasMenus) {
        var thisView = this
            , toolbar = thisView.toolbar
            , menus = toolbarData.menus || {};

        this.hasSubtrees = toolbarData.hasSubtrees || false;
        $.each(menus, function (menu_class, toolbartab) {
          // Render collapsible menus.
          var model = new _stb.SlogtbMenuModel({
            toolbar: toolbar,
            rootTermId: toolbarData.menutabs[toolbartab].rootTermId || 0,
            rootTermName: toolbarData.menutabs[toolbartab].rootTermName || 'Default',
            toolbartab: toolbartab
          });
          thisView.childViews[toolbartab + 'View'] = new _stb.SlogtbMenuView({
            el: thisView.$el.find('.' + menu_class).get(0),
            model: model,
            slogtbView: thisView,
            toolbartab: toolbartab
          });
        });
      }
    },
    initStatics: function (toolbarData) {
//      slogLog('_stb.SlogtbView.........initStatics: ' + this.toolbar);
      var $slogtbBar = this.$el
          , $bar = $slogtbBar.find('> .toolbar-bar')
          , $toolbarTabs = $bar.find('> .toolbar-tab')
          , $block = $bar.closest('.block')
          , $region = $block.closest('.region')
          , regionId = $region.attr('id')
          , mainModel = _stb.mainModel();
      // ensure region-id
      if (!regionId) {
        regionId = 'slogtb-region-' + this.toolbar;
        $region.attr('id', regionId);
      }

      var staticSettings = {
        tbView: this,
        $toolbarBar: $bar,
        toolbarBarLabel: toolbarData.tbBundleLabel,
        isVertical: toolbarData.isVertical,
        toolbarBarZIndex: parseInt($bar.css('zIndex')),
        $block: $block,
        $region: $region,
        regionId: regionId,
        $contentTarget: this.getContentTarget(),
        hasActions: false, // override later
        actionsWidth: 0
      };

      // init toolbar tabs
      staticSettings.tabs = [];
      var h = _sxt.helper
          , bCSS = h.showInvisibly($block)  // make unvisible and reset later
          , idx = 0
          , tabsFullWidth = 0
          , tabsVisibleWidth = 0
          , tbRegionData = this.mainModel.get('tbRegionData')
          , minWidth = $toolbarTabs.length * mainModel.stbMinWidth;

      this.tabsNumber = $toolbarTabs.length;
      $toolbarTabs.each(function () {
        var $this = $(this)
            , startWidth = $this.outerWidth()
            , prev = idx ? staticSettings.tabs[idx - 1] : false
            , id = $this.find('>a').attr('id');
        staticSettings.tabs[idx] = {
          $el: $this,
          idx: idx,
          id: id,
          height: $this.outerHeight(),
          top: prev ? prev.top + prev.height : 0,
          left: prev ? prev.left + prev.width : 0,
          startWidth: startWidth,
          width: startWidth,
          prev: prev,
          next: false
        };

        tabsFullWidth += startWidth;
        if (prev) {
          prev.next = staticSettings.tabs[idx];
        }

        idx++;
      });

      $block.css(bCSS); // restore visibility
      $region.css({position: 'relative', zIndex: 0});
      $block.css({width: '100%', minWidth: minWidth, padding: 0, border: 'none'});
      staticSettings.tabsFullWidth = tabsFullWidth;
      staticSettings.tabsIconizedWidth = minWidth;
      this.model.statics = staticSettings;

      tbRegionData[regionId] = tbRegionData[regionId] || {// ???
        regionId: regionId,
        $region: $region,
        tbViews: {},
        tabsFullWidth: 0,
        tabsIconizedWidth: 0,
        toolbarCount: 0
      };

      // region data
      // do not mix vertical and horizontal toolbars in one region.
      tabsVisibleWidth = this.hasMenus ? tabsFullWidth : 0;
      tbRegionData[regionId].lastTbWidth = tbRegionData[regionId].lastTbWidth || {};
      tbRegionData[regionId].lastTbWidth[this.toolbar] = tabsVisibleWidth;

      // for horizontal toolbar count full width in region, 
      // i.e. for all toolbars in the region.
      if (!staticSettings.isVertical && this.hasMenus) {
        tbRegionData[regionId].tabsFullWidth += tabsVisibleWidth;
        tbRegionData[regionId].tabsIconizedWidth += minWidth;
      }
      tbRegionData[regionId].tbViews[this.toolbar] = this;
      this.hasMenus && tbRegionData[regionId].toolbarCount++;
    },
    refreshBlockWidth: function () {
      var statics = this.model.statics
          , regionId = statics.regionId
          , tbRegionData = this.mainModel.get('tbRegionData')
          , rData = tbRegionData[regionId]
          , rtbLastWidth = tbRegionData[regionId].lastTbWidth[this.toolbar] || 0
          , tabsFullWidth = 0
          , tabsVisibleWidth = 0
          , minWidth;
      _.each(statics.tabs, function (tabData) {
        tabData.width = tabData.startWidth = tabData.$el.outerWidth();
        tabsFullWidth += tabData.width;
      });
      statics.tabsFullWidth = tabsFullWidth;
      if (rData) {
        tabsVisibleWidth = this.hasMenus ? tabsFullWidth : 0;
        tbRegionData[regionId].lastTbWidth[this.toolbar] = tabsVisibleWidth;
        rData.tabsFullWidth += tabsVisibleWidth - rtbLastWidth;
//        this.hasMenus && !!rtbLastWidth && rData.toolbarCount++;
        minWidth = rData.iconized ? rData.tabsIconizedWidth : statics.tabsFullWidth;
        statics.$block.css('min-width', minWidth + 2);
        this.mainView.updateRegionData();
      }
    },
    initDOM: function () {
      var $occur = $('.block-slogtb.stb-toolbar[toolbar="' + this.id + '"]')
          , $slogtbBar = this.$el
          , slogtbBarHeight = $slogtbBar.find('> .toolbar-bar').outerHeight()
          , st = this.model.statics;
      // test occurrences
      if ($occur.length > 1) {
        slogErr('Multiple occurrences of toolbar block: ' + this.id);
      }

      // ensure height, margin, ...
      $slogtbBar.height(slogtbBarHeight);
      st.isVertical && st.$block.height(slogtbBarHeight);
      $slogtbBar.parent().css('margin', 0);
      $slogtbBar.find('.toolbar-tray').addClass('toolbar-tray-vertical');
    },
    initIcon: function () {
      var bgUrl = this.$el.css('backgroundImage')
          , hasUrl = !!bgUrl && bgUrl !== 'none';
      this.tbIconUrl = hasUrl ? bgUrl : false;
      this.tbIconDarkUrl = hasUrl ? bgUrl.replace('css/icons/', 'css/icons/dark/') : false;
    },
    getContentTarget: function () {
      var $target = $('.block-slogtb.stb-content[toolbar="' + this.id + '"]');
      if ($target.length) {
        // test occurrences
        if ($target.length > 1) {
          slogErr('Multiple SlogTb content target: ' + this.id);
        }
        // content target found
        return $target;
      }

      // search default content target
      var contentTarget = _stb.settingsContentTarget()
          , $region = $(contentTarget);
      if (!$region.length) {
        slogErr('SlogTb content target not found: ' + this.id);
      }

      $target = $region;
      if ($target.hasClass('region')) {
        $target = $region.find('#slogtb-content-default');
        if (!$target.length) {
          var $lastTab = $region.find('.block-slogtb').last();
          $target = $('<div id="slogtb-content-default"></div>');
          $lastTab.length ? $lastTab.after($target) : $region.prepend($target);
        }
      }
      return $target;
    },
    onTrayClick: function (e) {
      if ($(e.target).hasClass('toolbar-handle')) {
        e.preventDefault();
        e.stopPropagation();
        this.adjustTrayPlacement();
      }
    },
    onTabClick: function (e) {
      e.preventDefault();
      e.stopPropagation();

      // If this tab has a tray associated with it, it is considered an
      // activatable tab.
      if (e.target.hasAttribute('data-toolbar-tray')) {
        var aTab = this.model.get('activeTab')
            , cTab = e.target
            , $cTab = $(cTab)
            , $topLiItems = this.getTray($cTab).find('li.menu-item.level-1').not('.level-2')
            , toolbartab = $cTab.attr('toolbartab')
            , $trayItems = this.getTrayItems($cTab)
            , aTabNew = (!aTab || cTab !== aTab) ? cTab : null;
        if ($cTab.hasClass('is-active')) {
          _stb.closeActiveToolbar();
          return;
        }
        aTabNew && _stb.closeActiveToolbar();
        _sxt.closeOpenPopup();

        if ($topLiItems.length === 1 && this.hasSubtrees) {
          $topLiItems.addClass('open');
        }

        this.mainModel.slogxtSetAndTrigger('change:tbTabClicked', null, this);
        if ($trayItems.length === 1 && !this.hasSubtrees) {
          // select the single item immediately
          var menuView = this.getMenuView(toolbartab);
          if (menuView) {
            if (!menuView.model.get('areSubtreesLoaded')) {
              menuView.model.slogxtReTrigger('change:subtrees');
            }
            menuView.activateTblineTab();
            $trayItems.first().click();
          }
        } else {
          this.model.set('activeTab', aTabNew);
        }

        if (_sxt.isDoNavigateActive()) {
          var donData = {
            toolbar: this.toolbar,
            toolbartab: toolbartab,
            targetKey: 'SlogtbTabItem',
            noServerResolve: true,
            entityId: $cTab.attr('roottermid'),
            label: $cTab.text(),
            info: $cTab.attr('title')
          };
          _sxt.setDoNavigateRawData(donData);
        }
      }
    },
    getTargetEntityId: function () {
      if (this.nodeInfo && !!this.nodeInfo.nodeId) {
        return this.nodeInfo.nodeId;
      }
      return 0;
    },
    closeActiveTab: function () {
//      slogLog('SlogtbView::.............closeActiveTab');
      this.model.set('activeTab', null);
    },
    onChangeActiveTab: function (model, activeTab) {
      activeTab && this.loadSubtrees();
      this.updateTabs();
    },
    onChangeActiveTray: function (model, activeTray) {
      var hasActiveTray = !!activeTray
          , activeView = hasActiveTray ? this : null
          , mainModel = _stb.mainModel();
      mainModel.set('activeSlogtbView', activeView);
      hasActiveTray && this.adjustTrayPlacement(model, activeTray);
    },
    onChangeContentData: function (model, data) {
      if (data.provider === 'slogtb') {
        model.statics.$contentTarget.html(data.content);
        model.statics.$contentTarget.show();
      }
    },
    onChangeMenuBlockData: function (model, data) {
      slogLog('SlogtbView: .......onChangeMenuBlockData..........onChangeMenuBlockData');
      var thisView = this;
      data.toolbar = this.toolbar;

      setTimeout($.proxy(function () {
        var tbView = this
            , tbMainModel = tbView.mainModel
            , toolbar = tbView.toolbar
            , $newEl = $(data.content).find('#slogtb-' + toolbar)
            , s = data.settings
            , rebuildState = {action: 'ondestroy', toolbar: toolbar};

        // trigger ondestroy and destroy views
        tbMainModel.set('rebuildRoleState', rebuildState);
        thisView.slogxtDestroy();

        // replace DOM element
        $newEl = thisView.$el.replaceWith($newEl);
        $newEl = $('#slogtb-' + toolbar);

        // recreate objects and attachments and trigger recreated
        tbView = _stb.createTbView(toolbar, $newEl, s.toolbarData);
        tbView.model.statics = tbMainModel.hookDataAlter('staticSettings', tbView.model.statics, true);
        rebuildState = {action: 'recreated', toolbar: toolbar, tbView: tbView};
        tbView.mainModel.set('rebuildRoleState', rebuildState);
        tbView.mainModel.unset('rebuildRoleState', {silent: true});
        // refresh
        s.toolbarData.hasMenus && tbView.refreshBlockWidth();
      }, this), 10);
    },
    /**
     * Updates the display of the tabs: toggles a tab and the associated tray.
     */
    updateTabs: function () {
      var $tab = $(this.model.get('activeTab'));
      // Deactivate the previous tab.
      $(this.model.previous('activeTab'))
          .removeClass('is-active')
          .prop('aria-pressed', false);
      // Deactivate the previous tray.
      $(this.model.previous('activeTray'))
          .removeClass('is-active');
      // Ensure tabs are visible
      this.model.previous('$el').find('.toolbar-tab').show();
      // Activate the selected tab.
      if ($tab.length > 0) {
        $tab.addClass('is-active')
            // Mark the tab as pressed.
            .prop('aria-pressed', true);
        // Activate the associated tray.
        var $tray = this.getTray($tab);
        if ($tray.length) {
          $tray.addClass('is-active');
          this.model.set('activeTray', $tray.get(0));
        } else {
          // There is no active tray.
          this.model.set('activeTray', null);
        }
      } else {
        // There is no active tray.
        this.model.set('activeTray', null);
      }
    },
    adjustTrayPlacement: function (model, activeTray) {
      model = model || this.model;
      activeTray = activeTray || model.get('activeTray');
      var st = model.statics
          , hasActiveTray = !!activeTray
          , $activeTray = $(activeTray);
      if (hasActiveTray && $activeTray.length) {
        var activeTab = this.model.get('activeTab')
            , $activeTab = $(activeTab)
            , openPopUp = this.id + ':' + $activeTab.attr('toolbartab')
            , winPos = _sxt.windowPosition($activeTab)
            , onRight = winPos.ratioX > 0.7
            , onBottom = winPos.ratioY > 0.7
            , mainModel = _stb.mainModel()
            , nOff = 30
            , minWidth = 240
            , maxWidth = onRight ? winPos.left - nOff : winPos.width - winPos.left - nOff
            , maxHeight = onBottom ? winPos.top - nOff : winPos.height - winPos.top - nOff
            , dataPlacement, css, position;

        if (st.isVertical) {
          position = {
            my: onRight ? 'right top' : 'left top',
            at: onRight ? 'left+10px top+10px' : 'right-10px top+10px',
            of: $activeTab,
            collision: "fit flip"
          };
        } else {
          st.multipleToolbars && st.$toolbarBar.width(st.$region.width());
          maxWidth = Math.max(minWidth, maxWidth);
          maxHeight -= $activeTab.height() + 20;
          position = {
            my: 'left top',
            at: 'left bottom',
            of: $activeTab,
            collision: "flipfit flip"
          };
        }

        css = {
          width: 'auto',
          height: 'auto',
          minHeight: Math.min(180, maxHeight),
          minWidth: Math.min(minWidth, maxWidth),
          maxHeight: maxHeight,
          maxWidth: maxWidth,
          zIndex: st.toolbarBarZIndex + 1
        };

        // hook für altering data placement
        dataPlacement = {
          view: this,
          toolbar: this.toolbar,
          statics: st,
          css: css,
          position: position
        };
        dataPlacement = mainModel.hookDataAlter('trayPlacement', dataPlacement, true);

        $activeTray.css(dataPlacement.css);
        $activeTray.position(dataPlacement.position);
        this.$el.sxtRegionToTop(true);
//        slogLog('SlogtbView.openPopUp::..........' + openPopUp);
        _sxt.screenModel().set('openPopUp', 'SlogtbView:' + openPopUp);
      }
    },
    adjustPlacement: function () {
      var $trays = this.$el.find('.toolbar-tray');
      if (!this.model.get('isOriented')) {
        $trays.css('margin-top', 0);
        $trays.removeClass('toolbar-tray-horizontal').addClass('toolbar-tray-vertical');
      } else {
        // The toolbar container is invisible. Its placement is used to
        // determine the container for the trays.
        $trays.css('margin-top', this.$el.find('.toolbar-bar').outerHeight());
      }
    },
    /**
     * Calls the endpoint URI that will return rendered subtrees with JSONP.
     *
     * The rendered slogtb menu subtrees HTML is cached on the client in
     * localStorage until the cache of the slogtb menu subtrees on the server-
     * side is invalidated. The subtreesHash is stored in localStorage as well
     * and compared to the subtreesHash in drupalSettings to determine when the
     * slogtb menu subtrees cache has been invalidated.
     */
    loadSubtrees: function () {
      var $activeTab = $(this.model.get('activeTab'))
          , toolbar = this.toolbar
          , toolbartab = $activeTab.attr('toolbartab')
          , toolbarData = this.model.get('toolbarData')
          , dataMenutab = toolbarData.menutabs[toolbartab] || {};
//todo::now - hasSubtrees
//      if (!dataMenutab.hasSubtrees) {
      if (!dataMenutab.subtreesHash) {
        return; // no subtrees to load
      }

      var targetterm = $activeTab.attr('targetterm')
          , rootterm = $activeTab.attr('rootterm')
          , roottermid = $activeTab.attr('roottermid')
          , menuView = _stb.getSlogtbMenuView(toolbar, toolbartab)
          , menuModel = menuView.model;
      // menuModel is required
      if (!menuModel) {
        return;
      }

      // Only load and render the slogtb menu subtrees if:
      //   (1) They have not been loaded yet.
      //   (2) The active tab is a slogtb menu tab, indicated by the
      //       presence of the data-drupal-subtrees attribute.
      if (!menuModel.get('areSubtreesLoaded') && $activeTab.data('drupal-subtrees') !== undefined) {
        var subtreesHash = dataMenutab.subtreesHash
            , keySubtreesHash = _sxt.storageKey('Slogtb', toolbar, toolbartab, roottermid, 'subtreesHash')
            , storageKey = _sxt.storageKey('Slogtb', toolbar, toolbartab, roottermid, 'subtrees')
            , cachedSubtreesHash = localStorage.getItem(keySubtreesHash)
            , cachedData = JSON.parse(localStorage.getItem(storageKey));
        // If we have the subtrees in localStorage and the subtree hash has not
        // changed, then use the cached data.        
//        cachedData = false; //todo::debugonly::disabled cache 
        if (subtreesHash === cachedSubtreesHash && cachedData && cachedData.subtrees) {
//          slogLog('SlogtbView........loadSubtrees... has cachedSubtrees');
          var menuView = _stb.getSlogtbMenuView(toolbar, toolbartab)
              , menuModel = menuView.model;
          menuModel.set('subtrees', cachedData.subtrees);
        }
        // Only make the call to get the subtrees if the orientation of the
        // toolbar is vertical.
        else {
//          slogLog('SlogtbView........loadSubtrees... ajax call');
          // Remove the cached menu information.
          localStorage.removeItem(keySubtreesHash);
          localStorage.removeItem(storageKey);

          var ajax = this.ajaxSubtrees
              , path = 'slogtb/xtajx/subtrees/' + toolbar + '/'
              + toolbartab + '/' + targetterm + '/' + rootterm + '/' + subtreesHash;
          ajax.url = ajax.options.url = Drupal.url(path);

          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = this.ajaxSubtreesResponse;
          ajax.slogxt.storageKey = storageKey;

          // ajax call by triggering slogtbSubtrees event
          // receiving response in this.ajaxSubtreesResponse
          // see Drupal.AjaxCommands.prototype.slogxtInvoke
          ajax.$xtelement.trigger('slogtbSubtrees');

          // Cache the hash for the subtrees locally.
          localStorage.setItem(keySubtreesHash, subtreesHash);
        }
      }
    },
    ajaxSubtreesResponse: function (ajax, response, status) {
      var data = response.data
          , toolbar = data.toolbar
          , toolbartab = data.toolbartab
          , storageKey = ajax.slogxt.storageKey
          , menuView = _stb.getSlogtbMenuView(toolbar, toolbartab);
      menuView.model.set('subtrees', data.subtrees);
      localStorage.setItem(storageKey, JSON.stringify(data));
    },
    getAjaxRebuild: function () {
      if (!this.ajaxRebuild) {
        this.ajaxRebuild = new _sxt.ajaxCreate(this.refreshEvent, this.$el);
      }
      return this.ajaxRebuild;
    },
    menuBlockRebuild: function (tbData) {
      this.showToolbar(false);
      !this.isTbSys && this.loadMenu(tbData);
    },
    loadMenu: function (tbData, reset) {
      function reloadToolbar() {
        slogLog('SlogtbView: reloadToolbar');
        thisView.loadNodeToolbar(tbData, true);
      }

      if (this.isTbSys) {
        return;
      }
      var thisView = this
          , tbHash = tbData.hash || false
          , rtIds = tbData.rtIds || ['none']
          , stbKey = 'rtIds.' + rtIds.join('-')
          , storageKey = _sxt.storageKey('stbToolbar', stbKey + '::data')
          , hashKey = _sxt.storageKey('stbToolbar', stbKey + '::hash');
      reset = !reset && (!tbHash || localStorage.getItem(hashKey) !== tbHash);
      if (!reset) {
        $.sxtIdbStorage.getItem({store: 'slogxtContent'}, storageKey)
            .fail(function (error, event) {
              reloadToolbar();
            })
            .done(function (result, event) {
              if (result) {
                var result = JSON.parse(result);
                thisView.model.set('menuBlockData', result);
              } else {
                reloadToolbar();
              }
            });
      } else {
        localStorage.removeItem(hashKey);

        var path = _sxt.baseAjaxPath('slogtb') + 'refresh/toolbar/' + this.toolbar
            , ajax = this.getAjaxRebuild();
        ajax.url = ajax.options.url = Drupal.url(path);
        ajax.slogxt.targetThis = this;

        ajax.slogxt.targetCallback = this.ajaxRebuildResponse;
        ajax.slogxt.storageKey = storageKey;
        ajax.slogxt.hashKey = hashKey;
        ajax.slogxt.hash = tbHash;

        // ajax call by triggering an event
        ajax.$xtelement.trigger(this.refreshEvent);
      }
    },
    ajaxRebuildResponse: function (ajax, response, status) {
      var sdata = JSON.stringify(response.data)
          , storageKey = ajax.slogxt.storageKey;
      // cache data in indexedDB storage, hash in localStorage
      $.sxtIdbStorage.setItem({store: 'slogxtContent'}, storageKey, sdata);
      localStorage.setItem(ajax.slogxt.hashKey, ajax.slogxt.hash);
      // set new data
      this.model.set('menuBlockData', response.data);
    },
    getMenuView: function (toolbartab) {
      return this.childViews[toolbartab + 'View'];
    },
    getTbBlock: function () {
      return this.model.statics.$block;
    },
    getTray: function ($tab) {
      return this.$el.find('#' + $tab.data('toolbarTray'));
    },
    getTrayItems: function ($tab) {
      return this.getTray($tab).find('a.slogtb-link-item');
    },
    showToolbar: function (show) {
      this.getTbBlock().toggleClass('visually-hidden', !show);
      if (this.hasMenus !== show) {
        this.hasMenus = show;
        this.refreshBlockWidth();
      }
    }
  };
  /**
   * Backbone view for the toolbar element.
   */
  _stb.SlogtbView = Backbone.View.extend(stbVisualViewExtensions);

})(jQuery, Drupal, drupalSettings, Backbone, localStorage, _);
