/**
 * @file
 * slogtb/js/slogtb.dvc.commands.js
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  var _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt
      , _xtg = Drupal.sxt_group;

  var tbCommands = {
    resolvePathTbMenuTid: function (key, tabData) { // called by _sxtContentForActiveTab
      function validate(data) {
//        slogLog('tbCommands.resolvePathTbMenuTid.......validate()');
        if (!!data.targetKey && data.targetKey === 'SlogtbMenuItem') {
          // reset
          this.donView.resetSelected(baseData, _sxt.invalidSel);

          // permissions
          if (!_sxt.isAdmin()) {
            var tbSysNode = _sxt.getTbSysNode()
                , testToolbar = data.toolbar;
            if (testToolbar === tbSysNode) {
              var nodeInfo = _stb.getSlogtbView(tbSysNode).nodeInfo || {};
              testToolbar = _stb.getPermBaseTbId(nodeInfo.baseTb);
            }
            if (testToolbar === 'role') {
              if (!_stb.hasPermRoleMenu(perms.role)) {
                return false;
              }
            } else if (testToolbar === 'user') {
              if (!perms.user) {  // determined serverside
                return false;
              }
            } else if (testToolbar === _sxt.getTbSysId()) {
              if (data.targetTb === _sxt.tokenTargetTb && !_stb.hasEditableToolbar()) {
                return false;
              } else if (data.targetTb !== _sxt.tokenTargetTb  // allow selection for __targettb__
                  && (!data.rolePerm || !_xtg.hasPermission(data.rolePerm))) {
                return false;
              }
            } else if (testToolbar === tbSysNode) {
              slogErr('Validate for toolbar: ' + tbSysNode);
              return false;
            } else if (!_sxt.hasPermToolbar(testToolbar)) {
              return false;
            }
          }

          // data
          !!data.noServerResolve && tabData.serverResolve && (tabData.serverResolve = false);
          !!data.serverResolve && (tabData.serverResolve = data.serverResolve);
          return (data.entityId != 0 || !!tabData.serverResolve);
        }
        return false;
      }

      var perms = tabData.resolveArgs ? tabData.resolveArgs.perms || {} : {}
      , args = tabData.resolveArgs ? tabData.resolveArgs[key] || {} : {}
      , btnTitle = Drupal.t('Select menu item')
          , xtTitle = args.xtTitle || btnTitle
          , xtInfo = args.xtInfo || ''
          , info, $doBtn;
      tabData['dialogTitle'] = tabData['actionTitle'] + ': ' + xtTitle;
      info = Drupal.t('Click the select button for selecting a menu item.') + '<br /><br />';
      info += Drupal.t('After clicking the button you may navigate the menues and select an item.');
      $doBtn = Drupal.theme.slogxtDialogDoButton(this.$activeContent, btnTitle, info, xtInfo);

      var baseData = {
        perms: perms,
        args: args,
        resolveKey: key,
        infoHint: btnTitle,
        targetKey: 'SlogtbMenuItem',
        validate: validate
      };
      $doBtn.on('click', $.proxy(this, '_sxtDonStart', baseData));
    },
    resolvePathTbTabId: function (key, tabData) { // called by _sxtContentForActiveTab
      function validate(data) {
//        slogLog('tbCommands.resolvePathTbTabId.......validate()');
        if (!!data.targetKey && data.targetKey === 'SlogtbTabItem') {
          this.donView.resetSelected(baseData, _sxt.invalidSel);

          if (!!args.isTarget && data.actionKey) {
            var sourceData = _sxt.getDonResultSource() || {}
            , toolbar_id = sourceData.toolbar // ???????
                , idx_resolve = toolbar_id + '::resolve' // resolve is set for toolbar _node
                , actionData = _sxt.getMainActionData(data.actionKey)
                , rootterm_ids = actionData.rootterm_ids || {}
            , targetTb = _sxt.isTbSysId(sourceData.toolbar) ? sourceData.targetTb : sourceData.toolbar
                , entityId = !!rootterm_ids[idx_resolve]
                ? rootterm_ids[idx_resolve]
                : rootterm_ids[targetTb] || false;
            data.targetTb = targetTb;
            data.entityId = entityId;
          }

          var tbSys = _sxt.getTbSysId()
              , tbSysNode = _sxt.getTbSysNode()
              , testToolbar = data.toolbar
              , sData = !!args.isTarget ? _sxt.getDonResultSource() || {} : {};
          if (testToolbar === tbSysNode) {
            var nodeInfo = _stb.getSlogtbView(tbSysNode).nodeInfo || {};
            testToolbar = _stb.getPermBaseTbId(nodeInfo.baseTb);
          }

          // permissions
          if (!_sxt.isAdmin()) {
            if (testToolbar === 'role') {
              if (!_stb.hasPermRoleMenu(perms.role)) {
                return false;
              }
            } else if (testToolbar === 'user') {
              if (!perms.user) {  // determined serverside
                return false;
              } else if (!!perms.slogtx && !_sxt.hasPermSlogtx(perms.slogtx)) {
                return false;
              }
            } else if (testToolbar === tbSys) {
              if (perms.role && perms.role === 'edit sxtrole-tbtab') {
                return false;
              }
              if (data.targetTb !== _sxt.tokenTargetTb  // allow for '__targettb__'
                  && !args.isTarget  // allow for move
                  && (!data.rolePerm || !_xtg.hasPermission(data.rolePerm))) {
                return false;
              }
            } else if (testToolbar === tbSysNode) {
              slogErr('Validate for toolbar: ' + tbSysNode);
              return false;
            } else if (!_sxt.hasPermToolbar(testToolbar)) {
              return false;
            } else if (!!perms.slogtx && !_sxt.hasPermSlogtx(perms.slogtx)) {
              return false;
            }
          }

          if (!!args.isTarget) {
            if (!sData.toolbar) {
              return false;
            } else if (data.prepareToolbar) {
              // nothing to do
            } else if (testToolbar === tbSys || sData.toolbar === tbSys) {
              var tToolbar = (testToolbar === tbSys) ? data.targetTb : testToolbar
                  , sToolbar = (sData.toolbar === tbSys) ? sData.targetTb : sData.toolbar;
              if (tToolbar !== sToolbar) {
                return false;
              } else if (testToolbar === tbSysNode && sData.rootTermId && data.entityId !== sData.rootTermId) {
                return false;
              }
            } else if (testToolbar !== sData.toolbar) {
              return false;
            } else if (sData.toolbar === tbSysNode && sData.rootTermId && data.entityId !== sData.rootTermId) {
              return false;
            }
          }
          !!data.noServerResolve && tabData.serverResolve && (tabData.serverResolve = false);
          !!data.serverResolve && (tabData.serverResolve = data.serverResolve);
          return (data.entityId != 0 || !!tabData.serverResolve);
        }
        return false;
      }

      var perms = tabData.resolveArgs ? tabData.resolveArgs.perms || {} : {}
      , args = tabData.resolveArgs ? tabData.resolveArgs[key] || {} : {}
      , btnTitle = Drupal.t('Select menu item')
          , xtTitle = args.xtTitle || btnTitle
          , xtInfo = args.xtInfo || ''
          , info, $doBtn;
      tabData['dialogTitle'] = tabData['actionTitle'] + ': ' + xtTitle;
      tabData.preserve = !!args.preserve;
      if (!!args.isTarget) {
        tabData.preserve = false;
        this.donResolveReset = {
          key: key,
          command: 'slogtb::resolvePathTbTabId'
        };
      }
      info = Drupal.t('Click the select button for selecting a toolbar item.') + '<br /><br />';
      info += Drupal.t('After clicking the button you may navigate and select an toolbar item.');
      $doBtn = Drupal.theme.slogxtDialogDoButton(this.$activeContent, btnTitle, info, xtInfo);

      var baseData = {
        perms: perms,
        args: args,
        resolveKey: key,
        infoHint: btnTitle,
        targetKey: 'SlogtbTabItem',
        validate: validate
      };
      $doBtn.on('click', $.proxy(this, '_sxtDonStart', baseData));
    },
    finishedTbMenuChange: function (args) {
      _stb.mainModel().slogxtSetAndTrigger('change:editedTbMenu', null, args);
      !!args.reload && this._sxtReload(args.reload);
    },
    finishedTbMenuAdd: function (args) {
      _stb.mainModel().slogxtSetAndTrigger('change:rearrangedTbMenu', null, args);
      !!args.reload && this._sxtReload(args.reload);
    },
    finishedTbMenuRearrange: function (args) {
      _stb.mainModel().slogxtSetAndTrigger('change:rearrangedTbMenu', null, args);
      !!args.reload && this._sxtReload(args.reload);
    },
    finishedTbMenuMove: function (args) {
      _stb.mainModel().slogxtSetAndTrigger('change:movedTbMenu', null, args);
      !!args.reload && this._sxtReload(args.reload);
    },
    finishedTbTabChange: function (args) {
      //todo::current::finishedTbTabChange() - test: is this handled ?
      _stb.mainModel().slogxtSetAndTrigger('change:editedTbTab', null, args);
      !!args.reload && this._sxtReload(args.reload);
    }
  };

  _sxt.dvc.commands.slogtb = tbCommands;

})(jQuery, Drupal, drupalSettings, Backbone, _);
