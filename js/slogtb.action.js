/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var _sxt = Drupal.slogxt;

  var mainActionsExt = {
    actionMenucreate: function () {
      if (_sxt.isDoNavigateActive()) {
        var donView = _sxt.getDoNavigateView()
            , baseData = donView.model.get('baseData') || {}
        , bArgs = baseData.args || {}
        , sourceData = donView.model.get('resultSource') || {}
        , sToolbar = sourceData.toolbar || false
            , testTb = sToolbar === _sxt.getTbSysId() ? sourceData.targetTb || false : sToolbar
            , pTbs = bArgs.preparableToolbars || {}
        , toolbarTarget = (!!testTb && !!pTbs[testTb]) ? pTbs[testTb] : false;

        if (baseData.targetKey === 'SlogtbTabItem' && bArgs.isMoveTarget && toolbarTarget) {
          var donData = {
            prepareToolbar: true,
            toolbar: testTb,
            targetKey: 'SlogtbTabItem',
            noServerResolve: true,
            entityId: toolbarTarget,
            label: bArgs.prepLabel,
            info: bArgs.prepInfo
          };
          _sxt.setDoNavigateRawData(donData);
        }
      }
    }
  };
  _sxt.MainActionsView.prototype.actions.slogtb = mainActionsExt;

})(jQuery, Drupal, drupalSettings);

